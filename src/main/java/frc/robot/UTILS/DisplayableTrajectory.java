package frc.robot.UTILS;

import edu.wpi.first.math.trajectory.Trajectory;

public class DisplayableTrajectory extends Trajectory {
    public String name;
    public Trajectory trajectory;

    public DisplayableTrajectory(String name, Trajectory trajectory) {
        this.name = name;
        this.trajectory = trajectory;
    }
}