package frc.robot.UTILS;

//Created by me in tutorialspoint.com's online java compiler and saved here

import java.util.ArrayList;

public class EZMaxExtensionAngleChecker {
    static double rightAngle = Math.PI * 0.5;
    static double minimumAngle, maximumAngle;
    static double maxHeight = 1.4986;
    static double maxLength = 1.6764;
    static double lengthToCheck = 1.68275;
    static ArrayList<Double> possibleAngles = new ArrayList<Double>();
    static int precisionSteps = 10000;
    static double minimumAngleDegrees = -20;
    static double maximumAngleDegrees = 116;
    static double degreesToCheck = Math.abs(minimumAngleDegrees) + Math.abs(maximumAngleDegrees);
    static double precision = degreesToCheck / precisionSteps;
    static int i;

    public static void main(String... args) {

        for (i = 0; i < precisionSteps; i++) {

            double angleToCheck = i * precision;
            angleToCheck -= minimumAngleDegrees;
            angleToCheck = (i * Math.PI) / 180;

            double inverseOfAngle = rightAngle - angleToCheck;
            double heightLimitedSide = maxHeight / Math.sin(angleToCheck);
            double lengthLimitedSide = maxLength / Math.sin(inverseOfAngle);
            double bestLength = (heightLimitedSide > lengthLimitedSide) ? lengthLimitedSide : heightLimitedSide;
            if (bestLength > lengthToCheck) {
                angleToCheck *= 180;
                angleToCheck /= Math.PI;
                possibleAngles.add(angleToCheck % 360);
            }
        }
        System.out.println("MinimumAngle = " + possibleAngles.get(0));
        System.out.println("MaximumAngle = " + possibleAngles.get(possibleAngles.size() - 1));
    }
}