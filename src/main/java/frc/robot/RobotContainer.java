package frc.robot;

import frc.robot.Constants.MovementConstants;
import frc.robot.Constants.OperatorPreferences;
import frc.robot.Constants.FieldPositionConstants.chargeStationPositions;
import frc.robot.UTILS.DisplayableTrajectory;
import frc.robot.commands.Arm.DIRECTDRIVE;
import frc.robot.commands.Arm.MoveLimited;
import frc.robot.commands.Arm.MovePredictive;
import frc.robot.commands.Arm.SetArm;
import frc.robot.commands.Arm.SetArmLow;
import frc.robot.commands.Arm.SetComplex;
import frc.robot.commands.Arm.SetLimited;
import frc.robot.commands.Arm.TestLimitsMovement;
import frc.robot.commands.Arm.LimitSwitchCommmands.LSExtend;
import frc.robot.commands.Arm.LimitSwitchCommmands.LSPitchFloor;
import frc.robot.commands.Arm.LimitSwitchCommmands.LSPitchStow;
import frc.robot.commands.Arm.LimitSwitchCommmands.LSRetract;
import frc.robot.commands.Auto.CreateAuto;
import frc.robot.commands.Auto.CreateAutoPaths;
import frc.robot.commands.Auto.DisplayAuto;
// import frc.robot.commands.Auto.CreateAuto;
// import frc.robot.commands.Auto.DisplayAuto;
import frc.robot.commands.Claw.CloseClaw;
import frc.robot.commands.Claw.OpenClaw;
import frc.robot.commands.Claw.ToggleClaw;
import frc.robot.commands.Drive.ArcadeDrive;
import frc.robot.commands.Drive.Balance;
import frc.robot.commands.Drive.CarDrive;
import frc.robot.commands.Drive.Drive;
import frc.robot.commands.Drive.DriveBackMeters;
import frc.robot.commands.Drive.DriveBackTimed;
import frc.robot.commands.Drive.FASTBalance;
import frc.robot.commands.Drive.FlipHeadingRobot;
import frc.robot.commands.Drive.FollowTrajectory;
import frc.robot.commands.Drive.SetRobotHeadingSuppplied;
import frc.robot.commands.Drive.SwerveBreak;
import frc.robot.commands.Drive.TankDrive;
import frc.robot.commands.Drive.ZeroHeading;
import frc.robot.commands.SmartCommands.BlindScore;
import frc.robot.subsystems.ArmSubsystem;
import frc.robot.subsystems.Camera;
import frc.robot.subsystems.ClawSubsystem;
import frc.robot.subsystems.DrivebaseSubsystem;
// import frc.robot.subsystems.DrivebaseSubsystem;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import java.sql.Driver;
import java.text.DateFormat.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BooleanSupplier;
import java.util.function.DoubleSupplier;
import java.util.function.Supplier;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.math.trajectory.TrajectoryConfig;
import edu.wpi.first.math.trajectory.TrajectoryGenerator;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Preferences;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.DriverStation.Alliance;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardLayout;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj.smartdashboard.Field2d;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.Commands;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.ParallelRaceGroup;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import edu.wpi.first.wpilibj2.command.button.POVButton;
import edu.wpi.first.wpilibj2.command.button.Trigger;

@SuppressWarnings("unused")

public class RobotContainer {
        public static enum ShuffleboardTabs {
                disabled(Shuffleboard.getTab("PlanAuto"), 0, "PlanAuto"),
                auto(Shuffleboard.getTab("ActiveAuto"), 1, "ActiveAuto"),
                tele(Shuffleboard.getTab("Tele-Op"), 2, "Tele-Op"),
                test(Shuffleboard.getTab("Test"), 3, "Test");

                ShuffleboardTab tab;
                int tabID;
                String name;

                ShuffleboardTabs(ShuffleboardTab tab, int tabID, String name) {
                        this.tab = tab;
                        this.tabID = tabID;
                        this.name = name;
                }
        }

        public Supplier<Double> verticalDriveSupplier,
                        horizontalDriveSupplier,
                        yawDriveSupplier,
                        altYawDriveSupplier,
                        armExtensionSupplier,
                        armPitchSupplier,
                        emptySupplier;

        public Supplier<Boolean> fieldOrientedSupplier,
                        fieldTurningSupplier,
                        driveSlowModeSupplier,
                        driveFastModeSupplier,
                        armSlowModeSupplier,
                        armFastModeSupplier,
                        complexUpper,
                        complexFloor,
                        emptyBooleanSupplier;

        public BooleanSupplier closeClawSupplier, openClawSupplier;

        public Supplier<Pose2d> chargeStationPoseSupplier;

        public String alliance;

        public final ArmSubsystem armSubsystem;
        public final ClawSubsystem clawSubsystem;
        public final DrivebaseSubsystem driveSubsystem;
        public final Camera camera;

        public DisplayAuto displayedValues;

        public Field2d fieldObject = new Field2d();
        // public ArrayList<DisplayableTrajectory> displayedTrajectories = new
        // ArrayList<DisplayableTrajectory>();

        private final XboxController driveController = new XboxController(OperatorPreferences.kDriverControllerPort);
        private final XboxController gunnerController = new XboxController(OperatorPreferences.kGunnerControllerPort);
        // private final XboxController gunnerController = driveController;

        public RobotContainer() {
                clawSubsystem = new ClawSubsystem();
                armSubsystem = new ArmSubsystem(clawSubsystem);
                driveSubsystem = new DrivebaseSubsystem(this);
                camera = new Camera();

                displayedValues = new DisplayAuto(this);

                configureBindings();

                SmartDashboard.putData(driveSubsystem);
                SmartDashboard.putData(fieldObject);

                Preferences.initDouble("Trigger Deadband", OperatorPreferences.kTriggerDeadband);
                Preferences.initDouble("Joystick Deadband", OperatorPreferences.kJoystickDeadband);
                Preferences.initDouble("Pitch Multiplyer", OperatorPreferences.kPitchSpeedMultiplyer);
                Preferences.initDouble("Extension Multiplyer", OperatorPreferences.kExtensionSpeedMultiplyer);
                Preferences.initDouble("Movement Acceleration MPS", OperatorPreferences.kAccelerationMPS);
                Preferences.initDouble("Movement Acceleration RPS", OperatorPreferences.kAccelerationRPS);
                Preferences.initDouble("Tele-Op Speed Multiplyer", OperatorPreferences.kTeleOPSpeedMultiplyer);
                Preferences.initDouble("Fast Speed Multiplyer", OperatorPreferences.kFastModeSpeedMultiplyer);
                Preferences.initDouble("Slow Speed Multiplyer", OperatorPreferences.kSlowModeSpeedMultiplyer);

                Preferences.getDouble("Trigger Deadband", OperatorPreferences.kTriggerDeadband);
                Preferences.getDouble("Joystick Deadband", OperatorPreferences.kJoystickDeadband);
                Preferences.getDouble("Pitch Multiplyer", OperatorPreferences.kPitchSpeedMultiplyer);
                Preferences.getDouble("Extension Multiplyer", OperatorPreferences.kExtensionSpeedMultiplyer);
                Preferences.getDouble("Movement Acceleration MPS", OperatorPreferences.kAccelerationMPS);
                Preferences.getDouble("Movement Acceleration RPS", OperatorPreferences.kAccelerationRPS);
                Preferences.getDouble("Tele-Op Speed Multiplyer", OperatorPreferences.kTeleOPSpeedMultiplyer);
                Preferences.getDouble("Fast Speed Multiplyer", OperatorPreferences.kFastModeSpeedMultiplyer);
                Preferences.getDouble("Slow Speed Multiplyer", OperatorPreferences.kSlowModeSpeedMultiplyer);
        }

        private void configureBindings() {
                verticalDriveSupplier = () -> (-driveController.getLeftY());
                horizontalDriveSupplier = () -> (driveController.getLeftX());
                yawDriveSupplier = () -> (driveController.getRightX());
                altYawDriveSupplier = () -> (-driveController.getRightY());
                fieldOrientedSupplier = () -> (driveController.getLeftBumperReleased());
                fieldTurningSupplier = () -> (driveController.getRightBumperReleased());
                driveFastModeSupplier = () -> (driveController
                                .getRightTriggerAxis() > Preferences.getDouble("Trigger Deadband",
                                                OperatorPreferences.kTriggerDeadband));
                driveSlowModeSupplier = () -> (driveController
                                .getLeftTriggerAxis() > Preferences.getDouble("Trigger Deadband",
                                                OperatorPreferences.kTriggerDeadband));

                armExtensionSupplier = () -> (-gunnerController.getLeftY());
                armPitchSupplier = () -> (-gunnerController.getRightY());
                armSlowModeSupplier = () -> (gunnerController
                                .getLeftTriggerAxis() > Preferences.getDouble("Trigger Deadband",
                                                OperatorPreferences.kTriggerDeadband));
                armFastModeSupplier = () -> (gunnerController
                                .getRightTriggerAxis() > Preferences.getDouble("Trigger Deadband",
                                                OperatorPreferences.kTriggerDeadband));
                // complexUpper = () -> (gunnerController.getLeftBumper());
                // complexFloor = () -> (gunnerController.getRightBumper());
                closeClawSupplier = () -> (gunnerController
                                .getRightTriggerAxis() > Preferences.getDouble("Trigger Deadband",
                                                OperatorPreferences.kTriggerDeadband));
                openClawSupplier = () -> (gunnerController.getLeftTriggerAxis() > Preferences
                                .getDouble("Trigger Deadband", OperatorPreferences.kTriggerDeadband));
                emptySupplier = () -> (0.0);
                emptyBooleanSupplier = ()->(false);


                driveSubsystem.setDefaultCommand(new Drive(
                                driveSubsystem,
                                verticalDriveSupplier,
                                horizontalDriveSupplier,
                                yawDriveSupplier,
                                altYawDriveSupplier,
                                emptyBooleanSupplier,
                                emptyBooleanSupplier,
                                driveFastModeSupplier,
                                driveSlowModeSupplier));

                // armSubsystem
                // .setDefaultCommand(new TestLimitsMovement(armSubsystem, armPitchSupplier,
                // armExtensionSupplier,
                // armFastModeSupplier, armSlowModeSupplier));

                armSubsystem.setDefaultCommand(new DIRECTDRIVE(
                                armSubsystem,
                                armPitchSupplier,
                                armExtensionSupplier,
                                armFastModeSupplier,
                                armSlowModeSupplier));

                // armSubsystem.setDefaultCommand(new MoveLimited(armSubsystem,
                // armPitchSupplier, armExtensionSupplier, armFastModeSupplier,
                // armSlowModeSupplier));

                new JoystickButton(driveController, XboxController.Button.kB.value)
                                .toggleOnTrue(new SequentialCommandGroup(new Balance(driveSubsystem,
                                                horizontalDriveSupplier, yawDriveSupplier, true, false),
                                                new SwerveBreak(driveSubsystem)));
                new JoystickButton(driveController, XboxController.Button.kX.value)
                                .onTrue(new SwerveBreak(driveSubsystem));
                new JoystickButton(driveController, XboxController.Button.kY.value)
                                .onTrue(new ZeroHeading(driveSubsystem));
                new JoystickButton(driveController, XboxController.Button.kA.value)
                                .onTrue(new ParallelRaceGroup(new FlipHeadingRobot(
                                                driveSubsystem, verticalDriveSupplier, horizontalDriveSupplier),
                                                new WaitCommand(2)));

                // new JoystickButton(driveController, XboxController.Button.kStart.value)
                // .onTrue(Commands.repeatingSequence(new
                // InstantCommand(driveSubsystem::zeroWheelDirection,
                // driveSubsystem)).withTimeout(2));

                // new JoystickButton(driveController,
                // XboxController.Button.kStart.value).toggleOnTrue(
                // new ArcadeDrive(driveSubsystem, verticalDriveSupplier, yawDriveSupplier));

                // new JoystickButton(driveController,
                // XboxController.Button.kStart.value).toggleOnTrue(
                // new CarDrive(driveSubsystem, verticalDriveSupplier, yawDriveSupplier));

                // new JoystickButton(driveController, XboxController.Button.kStart.value).toggleOnTrue(
                //                 new TankDrive(driveSubsystem, verticalDriveSupplier, altYawDriveSupplier));

                // new JoystickButton(gunnerController, XboxController.Button.kA.value)
                // .onTrue(new ToggleClaw(clawSubsystem));
                // new JoystickButton(gunnerController, XboxController.Button.kB.value)
                // .onTrue(new OpenClaw(clawSubsystem));
                // new JoystickButton(gunnerController, XboxController.Button.kX.value)
                // .onTrue(new CloseClaw(clawSubsystem));

                new JoystickButton(driveController, XboxController.Button.kY.value)
                                .onTrue(new InstantCommand(driveSubsystem::zeroEncoders));

                new Trigger(openClawSupplier).onTrue(new OpenClaw(clawSubsystem));
                new Trigger(closeClawSupplier).onTrue(new CloseClaw(clawSubsystem));

                // new JoystickButton(gunnerController, XboxController.Button.kStart.value)
                // .onTrue(new SetArmLow(armSubsystem));

                // new POVButton(gunnerController, 0).toggleOnTrue(new
                // LSPitchStow(armSubsystem));
                // new POVButton(gunnerController, 180).toggleOnTrue(new
                // LSPitchFloor(armSubsystem));

                // new POVButton(gunnerController, 90).toggleOnTrue(new LSExtend(armSubsystem));
                // new POVButton(gunnerController, 270).toggleOnTrue(new
                // LSRetract(armSubsystem));
                // new POVButton(gunnerController, 0).toggleOnTrue(new SetComplex(armSubsystem,
                // Units.inchesToMeters(0), 0, Units.inchesToMeters(8)));
                // new JoystickButton(gunnerController, XboxController.Button.kRightBumper.value)
                // .toggleOnTrue(
                //         new SetArm(armSubsystem, Units.inchesToMeters(15)));
                        
                //         new JoystickButton(gunnerController, XboxController.Button.kLeftBumper.value)
                //         .toggleOnTrue(
                //                 new SetArm(armSubsystem, Units.inchesToMeters(0)));

                new POVButton(gunnerController, 0)
                                .toggleOnTrue(new SetArm(armSubsystem, Rotation2d.fromDegrees(116), 0));
                new POVButton(gunnerController, 90).toggleOnTrue(
                                new SetArm(armSubsystem, Rotation2d.fromDegrees(33), Units.inchesToMeters(15)));
                new POVButton(gunnerController, 180)
                                .toggleOnTrue(new SetArm(armSubsystem, Rotation2d.fromDegrees(-20), 0));
                
                // new POVButton(gunnerController, 90)
                // .toggleOnTrue(new LSPitchFloor(armSubsystem));
                new POVButton(gunnerController, 270).toggleOnTrue(
                                new SetArm(armSubsystem, Rotation2d.fromDegrees(45), Units.inchesToMeters(15)));

                // Command turnToTarget = new SetRobotHeadingSuppplied(driveSubsystem,
                // () -> (Rotation2d.fromDegrees(getTargetYaw())),
                // verticalDriveSupplier, horizontalDriveSupplier);

                // new JoystickButton(gunnerController,
                // XboxController.Button.kRightBumper.value)
                // .onTrue(Commands.sequence(new InstantCommand(camera::detectAprilTag, camera),
                // turnToTarget))
                // .onFalse(new SequentialCommandGroup(
                // Commands.runOnce(()->{turnToTarget.cancel();}, driveSubsystem),
                // new InstantCommand(camera::driverMode,camera),
                // new InstantCommand(driveSubsystem::stopModules,driveSubsystem)));

        }

        // public Pose2d getStartingPose () {

        // }

        // public double getTargetYaw() {
        // camera.update();
        // camera.findBestTarget();
        // return camera.getYaw();
        // }

        public Command getAutonomousCommand() {

                // CreateAuto createdAuto = new CreateAuto(this, displayedValues, armSubsystem,
                // camera, clawSubsystem, driveSubsystem);
                // return createdAuto.getCreatedAuto();
                // Trajectory trajectory = TrajectoryGenerator.generateTrajectory(
                // new Pose2d(0,0,Rotation2d.fromDegrees(0)),
                // List.of(new Translation2d(-2, 1),
                // new Translation2d(-1, -1)),
                // new Pose2d(-5, 0, Rotation2d.fromDegrees(180)),
                // new TrajectoryConfig(MovementConstants.kPathingMaxSpeedMPS,
                // MovementConstants.kPathingMaxAccelerationMPS));

                // return new FollowTrajectory(driveSubsystem, trajectory);
                // return new FollowTrajectory(driveSubsystem,
                // displayedValues.getBaseTrajectories().get(1));
                CreateAuto auto = new CreateAuto(
                                this,
                                displayedValues,
                                armSubsystem,
                                camera,
                                clawSubsystem,
                                driveSubsystem);
                return auto.getCreatedAuto();

                // return new SequentialCommandGroup(
                // new InstantCommand(driveSubsystem::zeroHeading, driveSubsystem),
                // new CloseClaw(clawSubsystem),
                // new SetArm(armSubsystem, Rotation2d.fromDegrees(33),
                // Units.inchesToMeters(15)),
                // new OpenClaw(clawSubsystem),
                // new SetArm(armSubsystem, Rotation2d.fromDegrees(116), 0),

                // // new BlindScore(armSubsystem, clawSubsystem),
                // // new DriveBackMeters(driveSubsystem, Units.feetToMeters(18.8)),
                // new DriveBackTimed(driveSubsystem, 4.75),
                // // new LSPitchStow(armSubsystem)),
                // new ParallelRaceGroup(
                // new FlipHeadingRobot(driveSubsystem, emptySupplier, emptySupplier),
                // new WaitCommand(2)),
                // new InstantCommand(driveSubsystem::zeroHeading, driveSubsystem),
                // new LSPitchFloor(armSubsystem),
                // new CloseClaw(clawSubsystem),
                // new LSPitchStow(armSubsystem),
                // // new DriveBackMeters(driveSubsystem, Units.feetToMeters(18.8)),
                // new DriveBackTimed(driveSubsystem, 4.75));

                // CreateAuto createdAuto = new CreateAuto(this, displayedValues, armSubsystem,
                // camera, clawSubsystem,
                // driveSubsystem);
                // return createdAuto.getCreatedAuto();
        }

        // public DisplayAuto getDisplayedAuto() {
        // return displayedValues;
        // }

        public void displayPrimaryTrajectory(Trajectory trajectory) {
                fieldObject.getObject("primary").setTrajectory(trajectory);
                // displayedTrajectories.add(trajectory);
        }

        public void clearPrimaryTrajectory() {
                fieldObject.getObject("primary").setTrajectory(new Trajectory());
        }

        public void displayRobotPose(Pose2d robotPose) {
                fieldObject.setRobotPose(robotPose);
        }

        // @Deprecated
        // public void clearFieldMap() {
        // fieldObject = new Field2d();
        // }

        // public void clearAllTrajectories() {
        // for (int i = 0; i < displayedTrajectories.size(); i++) {
        // fieldObject.getObject(displayedTrajectories.get(i).name).setTrajectory(new
        // Trajectory());
        // }
        // }

        public void clearBaseTrajectories() {
                ArrayList<DisplayableTrajectory> baseTrajectories = displayedValues.getBaseTrajectories();

                for (int i = 0; i < baseTrajectories.size(); i++) {
                        fieldObject.getObject(baseTrajectories.get(i).name).setTrajectory(new Trajectory());
                }
        }

        public void displayBaseTrajectories() {
                ArrayList<DisplayableTrajectory> baseTrajectories = displayedValues.getBaseTrajectories();
                for (int i = 0; i < baseTrajectories.size(); i++) {
                        fieldObject.getObject(baseTrajectories.get(i).name)
                                        .setTrajectory(baseTrajectories.get(i).trajectory);
                }
        }

        // public void setShuffleboardTab(ShuffleboardTabs tab) {
        // try {
        // Shuffleboard.selectTab(tab.name);
        // } catch (Exception e) {
        // Shuffleboard.selectTab(tab.tabID);
        // }
        // }

        // public void addDoubleToTab(ShuffleboardTabs tab, String name, DoubleSupplier
        // number) {
        // tab.tab.addDouble(name, number);
        // }

}
