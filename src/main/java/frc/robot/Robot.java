package frc.robot;

import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import frc.robot.commands.Auto.Stop;
// import frc.robot.commands.Auto.DisplayAuto.Autons;
import frc.robot.commands.Drive.Balance;

public class Robot extends TimedRobot {
  private Command m_autonomousCommand;
  boolean connected = false;

  private RobotContainer m_robotContainer;

  @Override
  public void robotInit() {
    m_robotContainer = new RobotContainer();
    m_robotContainer.driveSubsystem.zeroEncoders();
    CameraServer.startAutomaticCapture();
  }

  @Override
  public void robotPeriodic() {
    CommandScheduler.getInstance().run();
  }

  @Override
  public void disabledInit() {
    // Shuffleboard.stopRecording();
    // Shuffleboard.selectTab(0);
    m_robotContainer.displayBaseTrajectories();

  }

  @Override
  public void disabledPeriodic() {
    // String alliance;
    // switch (DriverStation.getAlliance()) {
    // case Blue:
    // alliance = "blue";
    // break;

    // case Red:
    // alliance = "red";
    // break;

    // default:
    // alliance = "blue";
    // break;
    // }

    // m_robotContainer.driveSubsystem.resetOdometry(
    // Autons.valueOf(alliance +
    // m_robotContainer.displayedValues.startingPosition.getSelected()).startingPose);
    // ;

    // runs as soon as the driverstation is connected and only once
    // if (!connected && DriverStation.isDSAttached()) {
    // Shuffleboard.setRecordingFileNameFormat(DriverStation.getEventName() +
    // "_Match[" + DriverStation.getMatchNumber()
    // + "]_RandomID:" + (int) (Math.random() * 10000));
    // connected = true;
    // }

    if (!SmartDashboard.getBoolean("Move After Score", true)) {
      SmartDashboard.putBoolean("Balance After Move", false);
    }

  }

  @Override
  public void autonomousInit() {
    // Shuffleboard.startRecording();
    // Shuffleboard.selectTab(1);
    m_autonomousCommand = m_robotContainer.getAutonomousCommand();
    if (m_autonomousCommand != null) {
      m_autonomousCommand.schedule();
    }
  }

  @Override
  public void autonomousPeriodic() {
  }

  @Override
  public void teleopInit() {
    // m_robotContainer.driveSubsystem.addHeadingOffset(180);
    // Shuffleboard.startRecording();
    Shuffleboard.selectTab(2);
    m_robotContainer.clearBaseTrajectories();
    m_robotContainer.clearPrimaryTrajectory();
    // SmartDashboard.putData("Balance PID", Balance.yController);
    new Stop();

    if (m_autonomousCommand != null) {
      m_autonomousCommand.cancel();
    }
  }

  @Override
  public void teleopPeriodic() {
  }

  @Override
  public void testInit() {
    Shuffleboard.selectTab(3);
    CommandScheduler.getInstance().cancelAll();
  }

  @Override
  public void testPeriodic() {
  }

  @Override
  public void simulationInit() {
    DriverStation.silenceJoystickConnectionWarning(true);
  }

  @Override
  public void simulationPeriodic() {
  }
}
