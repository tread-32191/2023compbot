package frc.robot.commands.SmartCommands;

import java.util.function.Supplier;

import edu.wpi.first.math.geometry.Translation3d;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ArmSubsystem;
import frc.robot.subsystems.ClawSubsystem;
import frc.robot.subsystems.DrivebaseSubsystem;

@Deprecated
public class ScoreNearest extends CommandBase {
  private final ArmSubsystem armSubsystem;
  private final ClawSubsystem clawSubsystem;
  private final DrivebaseSubsystem driveSubsystem;
  private final Supplier<Boolean> upperSupplier, lowerSupplier;

  public enum ScoringHeight {
    High(new Translation3d(0, 0, 1)),
    Mid(new Translation3d(0, 0, 1)),
    Low(new Translation3d(0, 0, 1));

    Translation3d scoringHeight;

    ScoringHeight(Translation3d scoringHeight) {
      this.scoringHeight = scoringHeight;
    }
  }

  public ScoreNearest(
      ArmSubsystem armSubsystem,
      ClawSubsystem clawSubsystem,
      DrivebaseSubsystem driveSubsystem,
      Supplier<Boolean> upperSupplier,
      Supplier<Boolean> lowerSupplier) {

    this.armSubsystem = armSubsystem;
    this.clawSubsystem = clawSubsystem;
    this.driveSubsystem = driveSubsystem;
    this.upperSupplier = upperSupplier;
    this.lowerSupplier = lowerSupplier;
    addRequirements(armSubsystem, clawSubsystem, driveSubsystem);
  }

  @Override
  public void initialize() {
  }

  @Override
  public void execute() {
  }

  @Override
  public void end(boolean interrupted) {
  }

  @Override
  public boolean isFinished() {
    return false;
  }
}
