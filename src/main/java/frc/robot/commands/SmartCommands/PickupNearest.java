package frc.robot.commands.SmartCommands;

import java.util.function.Supplier;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ArmSubsystem;
import frc.robot.subsystems.ClawSubsystem;
import frc.robot.subsystems.DrivebaseSubsystem;

@Deprecated
public class PickupNearest extends CommandBase {
  private final ArmSubsystem armSubsystem;
  private final ClawSubsystem clawSubsystem;
  private final DrivebaseSubsystem driveSubsystem;

  public PickupNearest(
      ArmSubsystem armSubsystem,
      ClawSubsystem clawSubsystem,
      DrivebaseSubsystem driveSubsystem) {

    this.armSubsystem = armSubsystem;
    this.clawSubsystem = clawSubsystem;
    this.driveSubsystem = driveSubsystem;
    addRequirements(armSubsystem, clawSubsystem, driveSubsystem);
  }

  @Override
  public void initialize() {
  }

  @Override
  public void execute() {
  }

  @Override
  public void end(boolean interrupted) {
  }

  @Override
  public boolean isFinished() {
    return false;
  }
}
