package frc.robot.commands.SmartCommands;

import edu.wpi.first.wpilibj2.command.Commands;
import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.commands.Arm.LimitSwitchCommmands.LSPitchFloor;
import frc.robot.commands.Arm.QuickCommands.SetIntake;
import frc.robot.commands.Arm.QuickCommands.StowArm;
import frc.robot.commands.Claw.CloseClaw;
import frc.robot.commands.Claw.OpenClaw;
import frc.robot.subsystems.ArmSubsystem;
import frc.robot.subsystems.ClawSubsystem;

public class BlindIntake extends SequentialCommandGroup {
  public BlindIntake(ArmSubsystem armSubsystem, ClawSubsystem clawSubsystem) {
    addCommands(
        new ParallelCommandGroup(
            new OpenClaw(clawSubsystem),
            new LSPitchFloor(armSubsystem)),
            new ParallelCommandGroup(new CloseClaw(clawSubsystem)
        // new StowArm(armSubsystem)
        ));
        
  }
}
