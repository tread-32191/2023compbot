package frc.robot.commands.SmartCommands;

import edu.wpi.first.wpilibj2.command.Commands;
import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.commands.Arm.LimitSwitchCommmands.LSPitchFloor;
import frc.robot.commands.Arm.LimitSwitchCommmands.LSPitchStow;
import frc.robot.commands.Arm.QuickCommands.SetHighScore;
import frc.robot.commands.Arm.QuickCommands.StowArm;
import frc.robot.commands.Auto.Stop;
import frc.robot.commands.Claw.CloseClaw;
import frc.robot.commands.Claw.OpenClaw;
import frc.robot.subsystems.ArmSubsystem;
import frc.robot.subsystems.ClawSubsystem;

public class BlindScore extends SequentialCommandGroup {
  public BlindScore(ArmSubsystem armSubsystem, ClawSubsystem clawSubsystem) {
    addCommands(
        new ParallelCommandGroup(
            new CloseClaw(clawSubsystem),
            new LSPitchFloor(armSubsystem)
            // new SequentialCommandGroup(new WaitCommand(5)), new Stop()
            ),
        // new ParallelCommandGroup(
            new OpenClaw(clawSubsystem)
            // new LSPitchStow(armSubsystem)
            // )
        );
  }
}
