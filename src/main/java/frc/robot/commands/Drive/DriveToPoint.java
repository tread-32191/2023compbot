package frc.robot.commands.Drive;

import java.util.List;
import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.controller.ProfiledPIDController;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.math.trajectory.TrajectoryConfig;
import edu.wpi.first.math.trajectory.TrajectoryGenerator;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.SwerveControllerCommand;
import frc.robot.Constants.DrivebaseConstants;
import frc.robot.Constants.MovementConstants;
import frc.robot.subsystems.DrivebaseSubsystem;

public class DriveToPoint extends InstantCommand {
  DrivebaseSubsystem subsystem;
  Pose2d intendedPose;
  public static TrajectoryConfig trajectoryConfig = new TrajectoryConfig(
      MovementConstants.kPathingMaxSpeedMPS,
      MovementConstants.kPathingMaxAccelerationMPS)
      .setKinematics(DrivebaseConstants.kDriveKinematics);

  public DriveToPoint(DrivebaseSubsystem subsystem, Pose2d intendedPose) {
    this.subsystem = subsystem;
    this.intendedPose = intendedPose;
    addRequirements(subsystem);
  }

  @Override
  public void initialize() {
    Pose2d currentPose = subsystem.getPose();
    Trajectory trajectory = TrajectoryGenerator.generateTrajectory(
        currentPose, List.of(currentPose.interpolate(intendedPose, 0.5).getTranslation()), intendedPose,
        trajectoryConfig);

    PIDController xController = new PIDController(MovementConstants.kPathingXP, 0, 0);
    PIDController yController = new PIDController(MovementConstants.kPathingYP, 0, 0);
    ProfiledPIDController thetaController = new ProfiledPIDController(
        MovementConstants.kPathingThetaP, 0, 0, MovementConstants.kPathingThetaConstraints);
    thetaController.enableContinuousInput(-Math.PI, Math.PI);

    SwerveControllerCommand swerveControllerCommand = new SwerveControllerCommand(
        trajectory,
        subsystem::getPose,
        DrivebaseConstants.kDriveKinematics,
        xController,
        yController,
        thetaController,
        subsystem::setModuleStates,
        subsystem);

    new SequentialCommandGroup(
        new InstantCommand(() -> subsystem.resetOdometry(trajectory.getInitialPose())),
        swerveControllerCommand,
        new InstantCommand(() -> subsystem.stopModules()));

  }
}
