package frc.robot.commands.Drive;

import java.util.function.Supplier;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.filter.Debouncer;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants.DrivebaseConstants;
import frc.robot.subsystems.DrivebaseSubsystem;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class SetRobotHeadingSuppplied extends CommandBase {
  double yawHeading, yawGoal, outputSpeed;
  DrivebaseSubsystem subsystem;
  PIDController pidControl;
  ChassisSpeeds chassisSpeeds;
  Supplier<Double> xSpeed, ySpeed;
  Supplier<Rotation2d> setpoint;
  Debouncer debouncer = new Debouncer(0.1);

  public SetRobotHeadingSuppplied(DrivebaseSubsystem subsystem,
      Supplier<Rotation2d> setpoint,
      Supplier<Double> xSpeed,
      Supplier<Double> ySpeed) {
    this.subsystem = subsystem;
    this.xSpeed = xSpeed;
    this.ySpeed = ySpeed;
    this.setpoint = setpoint;
    pidControl = new PIDController(2.6, 0.15, 0.3);
    pidControl.setIntegratorRange(-1.5, 1.5);
    pidControl.enableContinuousInput(-Math.PI, Math.PI);
    pidControl.setTolerance(Units.degreesToRadians(1));
    SmartDashboard.putData(pidControl);
    addRequirements(subsystem);
  }

  @Override
  public void initialize() {
  }

  @Override
  public void execute() {
    pidControl.setSetpoint(MathUtil.angleModulus(setpoint.get().getRadians()));

    yawHeading = MathUtil.angleModulus(subsystem.getYaw().getRadians());
    SmartDashboard.putNumber("autoYaw", yawHeading);
    chassisSpeeds = ChassisSpeeds.fromFieldRelativeSpeeds(
        xSpeed.get() * 2,
        ySpeed.get() * 2,
        pidControl.calculate(yawHeading),
        Rotation2d.fromRadians(yawHeading));
    subsystem.setModuleStates(DrivebaseConstants.kDriveKinematics.toSwerveModuleStates(chassisSpeeds));
  }

  @Override
  public void end(boolean interrupted) {
  }

  @Override
  public boolean isFinished() {
    // return pidControl.atSetpoint();
    return false;
  }
}
