package frc.robot.commands.Drive;

import java.util.function.Supplier;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.filter.Debouncer;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants.DrivebaseConstants;
import frc.robot.Constants.MovementConstants;
import frc.robot.subsystems.DrivebaseSubsystem;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class FlipHeadingRobot extends CommandBase {
  double yawHeading, yawGoal, outputSpeed;
  DrivebaseSubsystem subsystem;
  PIDController pidControl;
  ChassisSpeeds chassisSpeeds;
  Supplier<Double> xSpeed, ySpeed;
  Debouncer debouncer = new Debouncer(0.1);

  public FlipHeadingRobot(DrivebaseSubsystem subsystem, Supplier<Double> xSpeed, Supplier<Double> ySpeed) {
    this.subsystem = subsystem;
    this.xSpeed = xSpeed;
    this.ySpeed = ySpeed;
    pidControl = new PIDController(1.9, 0.1, 0.2);
    pidControl.setIntegratorRange(-1.5, 1.5);
    pidControl.enableContinuousInput(-Math.PI, Math.PI);
    pidControl.setTolerance(Units.degreesToRadians(1));
    SmartDashboard.putData(pidControl);
    addRequirements(subsystem);
  }

  @Override
  public void initialize() {
    pidControl.setSetpoint(subsystem.getYaw().plus(Rotation2d.fromDegrees(180)).getRadians());

    // subsystem.zeroHeading();

  }

  @Override
  public void execute() {
    yawHeading = MathUtil.angleModulus(subsystem.getYaw().getRadians());
    // yawHeading = subsystem.getYaw().getRadians();
    SmartDashboard.putNumber("autoYaw", yawHeading);
    chassisSpeeds = ChassisSpeeds.fromFieldRelativeSpeeds(
        xSpeed.get() * 2,
        ySpeed.get() * 2,
        pidControl.calculate(yawHeading),
        Rotation2d.fromRadians(yawHeading));
    subsystem.setModuleStates(DrivebaseConstants.kDriveKinematics.toSwerveModuleStates(chassisSpeeds));

  }

  @Override
  public void end(boolean interrupted) {
    subsystem.stopModules();
    // subsystem.zeroHeading();
  }

  @Override
  public boolean isFinished() {
    return pidControl.atSetpoint();
    // return debouncer.calculate(pidControl.atSetpoint());
  }
}
