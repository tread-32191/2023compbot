package frc.robot.commands.Drive;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.networktables.Subscriber;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants.DrivebaseConstants;
import frc.robot.Constants.MovementConstants;
import frc.robot.subsystems.DrivebaseSubsystem;

public class DriveBackMeters extends CommandBase {
  ChassisSpeeds chassisSpeeds;
  DrivebaseSubsystem subsystem;
  double distanceGoal, distanceTraveled;
  PIDController distancePID;
  Pose2d startingPose, currentPose;

  public DriveBackMeters(DrivebaseSubsystem subsystem, double distanceGoal) {
    this.subsystem = subsystem;
    distancePID = new PIDController(1.0, 0.05, 0);
    distancePID.setTolerance(0.1524);
    distancePID.setIntegratorRange(0.5, 0.5);
    distancePID.setSetpoint(distanceGoal);
    addRequirements(subsystem);
  }

  @Override
  public void initialize() {
    startingPose = subsystem.getPose();
  }

  @Override
  public void execute() {
    currentPose = subsystem.getPose();
    distanceTraveled = currentPose.getTranslation().getDistance(startingPose.getTranslation());

    chassisSpeeds = new ChassisSpeeds(
        -distancePID.calculate(
             distanceTraveled),
             0,
             0);
    subsystem.setModuleStates(DrivebaseConstants.kDriveKinematics.toSwerveModuleStates(chassisSpeeds));
  }

  @Override
  public void end(boolean interrupted) {
    distancePID.reset();
    subsystem.stopModules();
  }

  @Override
  public boolean isFinished() {
    return distancePID.atSetpoint();
  }
}
