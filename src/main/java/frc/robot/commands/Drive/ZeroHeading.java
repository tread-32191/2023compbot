package frc.robot.commands.Drive;

import edu.wpi.first.wpilibj2.command.InstantCommand;
import frc.robot.subsystems.DrivebaseSubsystem;

public class ZeroHeading extends InstantCommand {
  DrivebaseSubsystem subsystem;

  public ZeroHeading(DrivebaseSubsystem subsystem) {
    this.subsystem = subsystem;

  }

  @Override
  public void initialize() {
    subsystem.zeroHeading();
  }
}
