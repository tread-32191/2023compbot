package frc.robot.commands.Drive;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.controller.ProfiledPIDController;
import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.SwerveControllerCommand;
import frc.robot.Constants.DrivebaseConstants;
import frc.robot.Constants.MovementConstants;
import frc.robot.subsystems.DrivebaseSubsystem;

public class FollowTrajectory extends InstantCommand {
  DrivebaseSubsystem subsystem;
  Trajectory trajectory;

  public FollowTrajectory(DrivebaseSubsystem subsystem, Trajectory trajectory) {
    this.subsystem = subsystem;
    this.trajectory = trajectory;
    addRequirements(subsystem);
  }

  @Override
  public void initialize() {
    PIDController xController = new PIDController(MovementConstants.kPathingXP, 0, 0);
    PIDController yController = new PIDController(MovementConstants.kPathingYP, 0, 0);
    ProfiledPIDController thetaController = new ProfiledPIDController(
        MovementConstants.kPathingThetaP, 0, 0, MovementConstants.kPathingThetaConstraints);
    thetaController.enableContinuousInput(-Math.PI, Math.PI);

    SwerveControllerCommand swerveControllerCommand = new SwerveControllerCommand(
        trajectory,
        subsystem::getPose,
        DrivebaseConstants.kDriveKinematics,
        xController,
        yController,
        thetaController,
        subsystem::setModuleStates,
        subsystem);

    new SequentialCommandGroup(
        new InstantCommand(() -> subsystem.resetOdometry(trajectory.getInitialPose())),
        swerveControllerCommand,
        new InstantCommand(() -> subsystem.stopModules()));

  }
}
