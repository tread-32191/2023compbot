package frc.robot.commands.Drive;

import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.networktables.Subscriber;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants.DrivebaseConstants;
import frc.robot.Constants.MovementConstants;
import frc.robot.subsystems.DrivebaseSubsystem;

public class DriveBackTimed extends CommandBase {
  ChassisSpeeds chassisSpeeds;
  DrivebaseSubsystem subsystem;
  double seconds;
  Timer timer;
  public DriveBackTimed(DrivebaseSubsystem subsystem, double seconds) {
    this.subsystem = subsystem;
    this.seconds = seconds;
    timer = new Timer();
    addRequirements(subsystem);
  }

  @Override
  public void initialize() {
    timer.start();
  }

  @Override
  public void execute() {
    // chassisSpeeds = new ChassisSpeeds(-1.0, 0, 0);
    chassisSpeeds = ChassisSpeeds.fromFieldRelativeSpeeds(-1.0, 0, 0, subsystem.getYaw());
    subsystem.setModuleStates(DrivebaseConstants.kDriveKinematics.toSwerveModuleStates(chassisSpeeds));
  }

  @Override
  public void end(boolean interrupted) {
    timer.stop();
    timer.reset();
    subsystem.stopModules();
  }

  @Override
  public boolean isFinished() {
    return timer.hasElapsed(seconds);
  }
}
