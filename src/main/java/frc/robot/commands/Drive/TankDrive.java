package frc.robot.commands.Drive;

import java.util.function.Supplier;
import edu.wpi.first.math.filter.SlewRateLimiter;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.kinematics.SwerveModuleState;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants.MovementConstants;
import frc.robot.Constants.OperatorPreferences;
import frc.robot.subsystems.DrivebaseSubsystem;

public class TankDrive extends CommandBase {
  private final DrivebaseSubsystem swerveSubsystem;
  private final Supplier<Double> leftSupplier, rightSupplier;
  private final SlewRateLimiter leftLimiter, rightLimiter;

  public TankDrive(
      DrivebaseSubsystem swerveSubsystem,
      Supplier<Double> leftSupplier,
      Supplier<Double> rightSupplier) {
    this.swerveSubsystem = swerveSubsystem;
    this.leftSupplier = leftSupplier;
    this.rightSupplier = rightSupplier;
    leftLimiter = new SlewRateLimiter(0.5);
    rightLimiter = new SlewRateLimiter(0.5);
    addRequirements(swerveSubsystem);
  }

  @Override
  public void initialize() {
  }

  @Override
  public void execute() {
    // Get the real-time joystick values
    double leftInput = leftSupplier.get();
    double rightInput = rightSupplier.get();

    // Apply a deadband to these values to keep the motors safe
    leftInput = Math.abs(leftInput) > OperatorPreferences.kTriggerDeadband
        ? leftInput
        : 0.0;
    rightInput = Math.abs(rightInput) > OperatorPreferences.kTriggerDeadband
        ? rightInput
        : 0.0;

    // Slow down the rate of change of speed to prevent damage to the motors and
    // tipping,
    // then multiply by speed multiplyers to get the target chassis speeds
    leftInput = leftLimiter.calculate(leftInput);
    rightInput = rightLimiter.calculate(rightInput);

    var speeds = DifferentialDrive.tankDriveIK(leftInput, rightInput, true);

    double leftSpeeds = speeds.left * 2;
    double rightSpeeds = speeds.right * 2;

    SwerveModuleState[] moduleStates = {
        new SwerveModuleState(
            leftSpeeds, Rotation2d.fromDegrees(0)),
        new SwerveModuleState(
            rightSpeeds, Rotation2d.fromDegrees(0)),
        new SwerveModuleState(
            leftSpeeds, Rotation2d.fromDegrees(0)),
        new SwerveModuleState(
            rightSpeeds, Rotation2d.fromDegrees(0))
    };

    // set the modules to their newly defined state
    swerveSubsystem.setModuleStates(moduleStates);
  }

  @Override
  public void end(boolean interrupted) {
    swerveSubsystem.stopModules();
  }

  @Override
  public boolean isFinished() {
    return false;
  }
}
