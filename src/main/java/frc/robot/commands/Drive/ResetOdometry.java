package frc.robot.commands.Drive;

import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import frc.robot.subsystems.DrivebaseSubsystem;

public class ResetOdometry extends InstantCommand {
  DrivebaseSubsystem driveSubsystem;
  Pose2d robotPose;

  public ResetOdometry(DrivebaseSubsystem driveSubsystem, Pose2d robotPose) {
    this.driveSubsystem = driveSubsystem;
    this.robotPose = robotPose;
    addRequirements(driveSubsystem);
  }

  @Override
  public void initialize() {
    driveSubsystem.resetOdometry(robotPose);
  }
}
