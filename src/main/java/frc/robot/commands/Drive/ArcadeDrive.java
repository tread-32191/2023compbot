package frc.robot.commands.Drive;

import java.util.function.Supplier;
import edu.wpi.first.math.filter.SlewRateLimiter;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.kinematics.SwerveModuleState;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants.MovementConstants;
import frc.robot.Constants.OperatorPreferences;
import frc.robot.subsystems.DrivebaseSubsystem;

public class ArcadeDrive extends CommandBase {
  private final DrivebaseSubsystem swerveSubsystem;
  private final Supplier<Double> movementInput, turningInput;
  private final SlewRateLimiter movementLimiter, turningLimiter;

  public ArcadeDrive(
      DrivebaseSubsystem swerveSubsystem,
      Supplier<Double> movementInput,
      Supplier<Double> turningInput) {
    this.swerveSubsystem = swerveSubsystem;
    this.movementInput = movementInput;
    this.turningInput = turningInput;
    movementLimiter = new SlewRateLimiter(0.5);
    turningLimiter = new SlewRateLimiter(0.5);
    addRequirements(swerveSubsystem);
  }

  @Override
  public void initialize() {
  }

  @Override
  public void execute() {
    // Get the real-time joystick values
    double movementInput = this.movementInput.get();
    double turningInput = this.turningInput.get();

    // Apply a deadband to these values to keep the motors safe
    movementInput = Math.abs(movementInput) > OperatorPreferences.kJoystickDeadband ? movementInput : 0.0;
    turningInput = Math.abs(turningInput) > OperatorPreferences.kJoystickDeadband ? turningInput : 0.0;

    // Slow down the rate of change of speed to prevent damage to the motors and
    // tipping,
    // then multiply by speed multiplyers to get the target chassis speeds
    movementInput = movementLimiter.calculate(movementInput);
    turningInput = turningLimiter.calculate(turningInput);

    var speeds = DifferentialDrive.arcadeDriveIK(movementInput, turningInput, true);

    double leftSpeeds = speeds.left * 2;
    double rightSpeeds = speeds.right * 2;

    SwerveModuleState[] moduleStates = {
        new SwerveModuleState(
            leftSpeeds, Rotation2d.fromDegrees(0)),
        new SwerveModuleState(
            rightSpeeds, Rotation2d.fromDegrees(0)),
        new SwerveModuleState(
            leftSpeeds, Rotation2d.fromDegrees(0)),
        new SwerveModuleState(
            rightSpeeds, Rotation2d.fromDegrees(0))
    };

    // set the modules to their newly defined state
    swerveSubsystem.setModuleStates(moduleStates);
  }

  @Override
  public void end(boolean interrupted) {
    swerveSubsystem.stopModules();
  }

  @Override
  public boolean isFinished() {
    return false;
  }
}
