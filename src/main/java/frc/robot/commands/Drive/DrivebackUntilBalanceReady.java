package frc.robot.commands.Drive;

import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants.DrivebaseConstants;
import frc.robot.subsystems.DrivebaseSubsystem;

public class DrivebackUntilBalanceReady extends CommandBase {
  ChassisSpeeds chassisSpeeds;
  DrivebaseSubsystem subsystem;
  double angle;

  public DrivebackUntilBalanceReady(DrivebaseSubsystem subsystem) {
    this.subsystem = subsystem;
    addRequirements(subsystem);
  }

  @Override
  public void initialize() {
  }

  @Override
  public void execute() {
    angle = subsystem.getPitch().getDegrees();
    chassisSpeeds = new ChassisSpeeds(-0.5, 0, 0);
    subsystem.setModuleStates(DrivebaseConstants.kDriveKinematics.toSwerveModuleStates(chassisSpeeds));
  }

  @Override
  public void end(boolean interrupted) {
    subsystem.stopModules();
  }

  @Override
  public boolean isFinished() {
    return Math.abs(angle) > 5;
  }
}
