package frc.robot.commands.Drive;

import edu.wpi.first.math.filter.Debouncer;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DrivebaseSubsystem;

public class SwerveBreak extends CommandBase {
  DrivebaseSubsystem subsystem;
  boolean isFinished;
  Debouncer finishedDebouncer;

  public SwerveBreak(DrivebaseSubsystem subsystem) {
    this.subsystem = subsystem;
    finishedDebouncer = new Debouncer(0.1);
    isFinished = false;
    addRequirements(subsystem);
  }

  @Override
  public void initialize() {
    subsystem.stopModules();
  }

  @Override
  public void execute() {
    subsystem.setForceBreak();
    isFinished = finishedDebouncer.calculate(subsystem.modulesAtTurningSetpoint());

  }

  @Override
  public void end(boolean interrupted) {
    subsystem.stopModules();
  }

  @Override
  public boolean isFinished() {
    return isFinished;
  }
}
