package frc.robot.commands.Drive;

import java.util.function.Supplier;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.filter.SlewRateLimiter;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.kinematics.SwerveModuleState;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants.DrivebaseConstants;
import frc.robot.Constants.MovementConstants;
import frc.robot.Constants.OperatorPreferences;
import frc.robot.subsystems.DrivebaseSubsystem;

public class Drive extends CommandBase {
  private final DrivebaseSubsystem swerveSubsystem;
  private final Supplier<Double> xSpeedFunction, ySpeedFunction, turningSpeedFunction, altTurningFunction;
  private Supplier<Boolean> fieldOrientedFunction, fieldTurningSupplier, boostSpeedFunction, slowSpeedFunction;
  private final SlewRateLimiter xLimiter, yLimiter, thetaLimiter;
  private double positionalSpeed, rotationalSpeed;
  private boolean isFieldOriented = true;
  private boolean isFieldTurning = false;
  private PIDController turnControl;

  public Drive(
      DrivebaseSubsystem swerveSubsystem,
      Supplier<Double> xSpeedFunction,
      Supplier<Double> ySpeedFunction,
      Supplier<Double> turningSpeedFunction,
      Supplier<Double> altTurningFunction,
      Supplier<Boolean> fieldOrientedFunction,
      Supplier<Boolean> fieldTurningSupplier,
      Supplier<Boolean> boostSpeedFunction,
      Supplier<Boolean> slowSpeedFunction) {

    this.swerveSubsystem = swerveSubsystem;
    this.xSpeedFunction = xSpeedFunction;
    this.ySpeedFunction = ySpeedFunction;
    this.turningSpeedFunction = turningSpeedFunction;
    this.altTurningFunction = altTurningFunction;
    this.fieldOrientedFunction = fieldOrientedFunction;
    this.fieldTurningSupplier = fieldTurningSupplier;
    this.boostSpeedFunction = boostSpeedFunction;
    this.slowSpeedFunction = slowSpeedFunction;
    turnControl = new PIDController(0.9, 0.1, 0.25);
    turnControl.setIntegratorRange(-1.5, 1.5);
    turnControl.enableContinuousInput(-Math.PI, Math.PI);
    turnControl.setTolerance(Units.degreesToRadians(1));
    xLimiter = new SlewRateLimiter(MovementConstants.kAccelerationMPS);
    yLimiter = new SlewRateLimiter(MovementConstants.kAccelerationMPS);
    thetaLimiter = new SlewRateLimiter(MovementConstants.kAccelerationRPS);

    addRequirements(swerveSubsystem);
  }

  @Override
  public void initialize() {
    SmartDashboard.putBoolean("driveable", true);
  }

  @Override
  public void execute() {
    Rotation2d robotAngle = swerveSubsystem.getYaw();

    // Get the real-time joystick values
    double xSpeed = xSpeedFunction.get();
    double ySpeed = ySpeedFunction.get();

    // isFieldTurning = (fieldTurningSupplier.get()) ? !isFieldTurning : isFieldTurning;
    // isFieldOriented = (fieldOrientedFunction.get()) ? !isFieldOriented : isFieldOriented;

    // isFieldOriented

    double thetaSpeed;
    if (isFieldTurning && isFieldOriented) {
      double angle = MathUtil.angleModulus(robotAngle.getRadians());
      if (((turningSpeedFunction.get() * turningSpeedFunction.get())
          + (altTurningFunction.get() * altTurningFunction.get())) > OperatorPreferences.kTriggerDeadband) {
        thetaSpeed = turnControl.calculate(angle, Math.atan2(turningSpeedFunction.get(), altTurningFunction.get()));
        thetaSpeed = MathUtil.clamp(thetaSpeed, -1, 1);
      } else {
        thetaSpeed = 0;
      }
    } else {
      thetaSpeed = turningSpeedFunction.get();
    }

    // Apply a deadband to these values to keep the motors safe
    xSpeed = Math.abs(xSpeed) > OperatorPreferences.kTriggerDeadband ? xSpeed
        : 0.0;
    ySpeed = Math.abs(ySpeed) > OperatorPreferences.kTriggerDeadband ? ySpeed
        : 0.0;
    thetaSpeed = Math.abs(thetaSpeed) > OperatorPreferences.kTriggerDeadband
        ? thetaSpeed
        : 0.0;

    // calculate the output speed based
    if (slowSpeedFunction.get()) {
      positionalSpeed = MovementConstants.kSlowModeSpeedMPS;
      rotationalSpeed = MovementConstants.kSlowModeSpeedRPS;

    } else if (boostSpeedFunction.get()) {
      positionalSpeed = MovementConstants.kFastModeSpeedMPS;
      rotationalSpeed = MovementConstants.kFastModeSpeedRPS;

    } else {
      positionalSpeed = MovementConstants.kTeleOpMaxSpeedMPS;
      rotationalSpeed = MovementConstants.kTeleOpMaxSpeedRPS;

    }

    // Slow down the rate of change of speed to prevent damage to the motors and
    // tipping,
    // then multiply by speed multiplyers to get the target chassis speeds
    xSpeed = xLimiter.calculate(xSpeed) * positionalSpeed;
    ySpeed = yLimiter.calculate(ySpeed) * positionalSpeed;
    thetaSpeed = thetaLimiter.calculate(thetaSpeed) * rotationalSpeed;

    SmartDashboard.putNumber("xSpeed", xSpeed);

    ChassisSpeeds chassisSpeeds;

    // convert the speed values into chassis speeds for the robot to try to do


    if (isFieldOriented) {
      SmartDashboard.putString("driveType", "field");
      chassisSpeeds = ChassisSpeeds.fromFieldRelativeSpeeds(xSpeed, ySpeed, thetaSpeed, robotAngle);
      // chassisSpeeds = new ChassisSpeeds(
      // -ySpeed * robotAngle.getCos() + xSpeed * robotAngle.getSin(),
      // -ySpeed * robotAngle.getSin() + -xSpeed * robotAngle.getCos(), // -Yspeed
      // thetaSpeed);
    } else {
      SmartDashboard.putString("driveType", "chassis");
      chassisSpeeds = new ChassisSpeeds(xSpeed, ySpeed, thetaSpeed);
    }
    // unpack the robot chassis speeds into individual module states
    SwerveModuleState[] moduleStates = DrivebaseConstants.kDriveKinematics.toSwerveModuleStates(chassisSpeeds);
    // set the modules to their newly defined state
    swerveSubsystem.setModuleStates(moduleStates);
  }

  @Override
  public void end(boolean interrupted) {
    SmartDashboard.putBoolean("driveable", false);
    swerveSubsystem.stopModules();
  }

  @Override
  public boolean isFinished() {
    return false;
  }
}
