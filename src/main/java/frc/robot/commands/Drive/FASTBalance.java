// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.Drive;

import java.io.Console;
import java.util.function.Supplier;

import edu.wpi.first.math.filter.Debouncer;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.kinematics.SwerveDriveKinematics;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants.DrivebaseConstants;
import frc.robot.subsystems.DrivebaseSubsystem;

public class FASTBalance extends CommandBase {
  /** Creates a new FASTBalance. */
  DrivebaseSubsystem subsystem;
  boolean tiltingStarted, pitchDecreasing;
  double lastPitch;
  ChassisSpeeds chassisSpeeds;
  Supplier<Double> strafeFunction, thetaFunction;
      boolean autoFinish;
      Debouncer debouncer;

  public FASTBalance(DrivebaseSubsystem subsystem, Supplier<Double> strafeFunction, Supplier<Double> thetaFunction,
  boolean autoFinish) {
    this.subsystem = subsystem;
    this.strafeFunction = strafeFunction;
    this.thetaFunction = thetaFunction;
    this.autoFinish = autoFinish;
    debouncer = new Debouncer(0.04);
    addRequirements(subsystem);

    // Use addRequirements() here to declare subsystem dependencies.
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    
    pitchDecreasing = false;
    tiltingStarted = false;
    lastPitch = 0.0;
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {

    double pitch = subsystem.getPitch().getDegrees();
    pitchDecreasing = Math.abs(pitch) < Math.abs(lastPitch) && pitch>5;
    System.out.println("currentPitch: "+pitch+" lastPitch: " + lastPitch + "pitchDecreasing: "+pitchDecreasing);
    pitchDecreasing = debouncer.calculate(pitchDecreasing);


    //if pitch hasnt started move chassis forward
    //if pitch has started tilting and hasnt started dropping balance p
    //if pitch has started dropping stop
    
  

    if (Math.abs(pitch)<13) {
      chassisSpeeds = new ChassisSpeeds(0.55, 0, 0);
      // lastPitch = 0;
    } else {
      chassisSpeeds = new ChassisSpeeds(0.25, 0, 0);
      // lastPitch = pitch;

    }
      lastPitch = pitch;

    subsystem.setModuleStates(DrivebaseConstants.kDriveKinematics.toSwerveModuleStates(chassisSpeeds));

  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    subsystem.stopModules();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    // return debouncer.calculate(pitchDecreasing);
    return pitchDecreasing;
    // return false;
  }
}
