package frc.robot.commands.Drive;

import java.util.function.Supplier;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.estimator.KalmanFilter;
import edu.wpi.first.math.filter.Debouncer;
import edu.wpi.first.math.filter.SlewRateLimiter;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.kinematics.SwerveModuleState;
import edu.wpi.first.math.system.LinearSystem;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants.DrivebaseConstants;
import frc.robot.Constants.MovementConstants;
import frc.robot.Constants.OperatorPreferences;
import frc.robot.subsystems.DrivebaseSubsystem;

public class Balance extends CommandBase {

  private final DrivebaseSubsystem subsystem;
  Rotation2d yaw;
  double pitch, roll,
      tiltingDirection,
      xSpeed, ySpeed, thetaSpeed;
  PIDController xController;
  public static PIDController yController;
  PIDController tController;

  Supplier<Double> thetaFunction, strafeFunction;
  SlewRateLimiter thetaLimiter, strafeLimiter;
  Debouncer finishDebouncer;
  boolean autoFinish;
  boolean reversed;

  public Balance(DrivebaseSubsystem subsystem, Supplier<Double> strafeFunction, Supplier<Double> thetaFunction,
      boolean autoFinish, boolean reversed) {
    this.subsystem = subsystem;
    this.reversed = reversed;
    yController = new PIDController(MovementConstants.kBalanceP, MovementConstants.kBalanceI,
        MovementConstants.kBalanceD);
    yController.setIntegratorRange(-0.1, 0.1);
    yController.setTolerance(Units.degreesToRadians(2));
    this.thetaLimiter = new SlewRateLimiter(MovementConstants.kAccelerationRPS);
    this.thetaFunction = thetaFunction;
    yController.setSetpoint(0);
    this.strafeLimiter = new SlewRateLimiter(MovementConstants.kAccelerationMPS);
    this.strafeFunction = strafeFunction;
    this.autoFinish = autoFinish;
    finishDebouncer = new Debouncer(1.5);
    addRequirements(subsystem);
  }

  @Override
  public void initialize() {
    subsystem.stopModules();
  }

  @Override
  public void execute() {
    pitch = subsystem.getPitch().getRadians(); // [-PI/12,PI/12]
    roll = subsystem.getRoll().getRadians(); // [-PI/12,PI/12]
    // yaw = subsystem.getYaw();// [-PI,PI]

    // tiltingDirection = Math.atan2(pitch, roll);
    // tiltingDirection = Units.radiansToDegrees(tiltingDirection);
    // tiltingDirection -= 180;
    // tiltingDirection = (Math.abs(tiltingDirection) < 90) ? -1 : 1;

    double xSpeed = strafeFunction.get();
    // double ySpeed = (Math.sqrt((pitch * pitch) + (roll * roll)));
    // ySpeed *= tiltingDirection;
    double ySpeed = ((Math.sqrt(pitch*pitch)+(roll*roll)));
    double thetaSpeed = thetaFunction.get();

    xSpeed = Math.abs(xSpeed) > OperatorPreferences.kTriggerDeadband ? xSpeed
        : 0.0;
    ySpeed = (Math.abs(ySpeed) > MovementConstants.kTiltTolerance) ? ySpeed : 0.0;
    thetaSpeed = Math.abs(thetaSpeed) > OperatorPreferences.kTriggerDeadband
        ? thetaSpeed
        : 0.0;

    ySpeed *= MovementConstants.kConvertTiltToInput;

    xSpeed = strafeLimiter.calculate(xSpeed) * MovementConstants.kPhysicalMaxSpeedMPS;
    ySpeed = yController.calculate(ySpeed) * MovementConstants.kPhysicalMaxSpeedMPS;
    thetaSpeed = thetaLimiter.calculate(thetaSpeed) * MovementConstants.kPhysicalMaxSpeedRPS;
    Rotation2d angle = subsystem.getYaw();
    double angleDegrees = angle.getDegrees() % 360;
    boolean facingForward = angleDegrees < 90 || angleDegrees > 270;

    if (facingForward) {}
    ChassisSpeeds chassisSpeeds = new ChassisSpeeds(ySpeed, xSpeed, thetaSpeed);
    subsystem.setModuleStates(DrivebaseConstants.kDriveKinematics.toSwerveModuleStates(chassisSpeeds));
  }

  @Override
  public void end(boolean interrupted) {
    subsystem.stopModules();
  }

  public double getRemainingPeriodTimeInSeconds() {
    return DriverStation.getMatchTime();
  }

  @Override
  public boolean isFinished() {
    return finishDebouncer.calculate(yController.atSetpoint()) && autoFinish;
  }
}
