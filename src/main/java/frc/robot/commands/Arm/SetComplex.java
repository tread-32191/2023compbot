package frc.robot.commands.Arm;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants.ManipulatorConstants;
import frc.robot.Constants.RobotPositionConstants;
import frc.robot.subsystems.ArmSubsystem;

public class SetComplex extends CommandBase {
  ArmSubsystem subsystem;
  double targetX, targetY, distanceToCenterOfGamePieceFromEndOfExtension, absX, absY, absN, absR,
      pitchPID, pitchFFV, extensionPID, targetPitch, targetExtension, currentPitch, maxCurrentExtension,
      currentExtensionTarget;
  Translation2d absoluteVector, scoreVector, relativeVector;
  boolean isArmAtSetpoint = false;

  public SetComplex(ArmSubsystem subsystem, Rotation2d pitch) {
    addRequirements(subsystem);
    Translation2d mockTargetTranslation = new Translation2d(
        RobotPositionConstants.kClawMaximumLength + RobotPositionConstants.kPivotArmLengthFromPivotPoint
            - (RobotPositionConstants.kClawMaximumLength - distanceToCenterOfGamePieceFromEndOfExtension),
        pitch);
    double targetX = mockTargetTranslation.getX();
    double targetY = mockTargetTranslation.getY();
    new SetComplex(subsystem, targetX, targetY, distanceToCenterOfGamePieceFromEndOfExtension);
  }

  public SetComplex(ArmSubsystem subsystem,
      double targetX, double targetY,
      double distanceToCenterOfGamePieceFromEndOfExtension) {
    this.subsystem = subsystem;
    this.targetX = targetX;
    this.targetY = targetY;
    this.distanceToCenterOfGamePieceFromEndOfExtension = distanceToCenterOfGamePieceFromEndOfExtension;
    addRequirements(subsystem);

    // create an object that represents the translation from the center of the pivot
    // point to the targetObject's center point
    scoreVector = new Translation2d(targetX, targetY);

    // create an object that represents the translation from the center of the pivot
    // point to the end of the claw
    absoluteVector = scoreVector.plus(
        new Translation2d(RobotPositionConstants.kClawMaximumLength - distanceToCenterOfGamePieceFromEndOfExtension,
            scoreVector.getAngle()));

    // check to see if the arm would leave the specified zone that the robot must
    // operate in, and fix it if it won't
    absX = absoluteVector.getX();
    absY = absoluteVector.getY();
    absX = MathUtil.clamp(absX, RobotPositionConstants.kMinXFromPivot, RobotPositionConstants.kMaxXFromPivot);
    absY = MathUtil.clamp(absY, RobotPositionConstants.kMinYFromPivot, RobotPositionConstants.kMaxYFromPivot);
    absoluteVector = new Translation2d(absX, absY);

    // check to see if the arm can physically get to the target point
    absR = absoluteVector.getAngle().getRadians();
    absN = absoluteVector.getNorm();
    absR = MathUtil.clamp(absR, ManipulatorConstants.kMinPitch, ManipulatorConstants.kMaxPitch);
    absN = MathUtil.clamp(absN,
        RobotPositionConstants.kPivotArmLengthFromPivotPoint + ManipulatorConstants.kMinExtension
            + RobotPositionConstants.kClawMaximumLength,
        RobotPositionConstants.kPivotArmLengthFromPivotPoint + ManipulatorConstants.kMaxExtension
            + RobotPositionConstants.kClawMaximumLength);
    absoluteVector = new Translation2d(absN, Rotation2d.fromRadians(absR));

    // create an object that holds the values that the arm must operate with to get
    // to the target point
    relativeVector = absoluteVector.minus(
        new Translation2d(
            RobotPositionConstants.kClawMaximumLength + RobotPositionConstants.kPivotArmLengthFromPivotPoint,
            absoluteVector.getAngle()));
  }

  @Override
  public void initialize() {
    targetPitch = relativeVector.getAngle().getRadians();
    targetExtension = relativeVector.getNorm();
  }

  @Override
  public void execute() {
    currentPitch = subsystem.getPitchEncoderRadians();
    maxCurrentExtension = subsystem.findMaxExtensionAbsolute(currentPitch);
    currentExtensionTarget = MathUtil.clamp(targetExtension, 0, maxCurrentExtension);

    pitchPID = subsystem.getPitchPID(targetPitch);
    pitchFFV = subsystem.getPitchFFV(targetPitch, 0);

    subsystem.setPitchMotorVoltage(pitchFFV + pitchPID);
    if (subsystem.isExtensionHelpful(currentPitch)) {
      extensionPID = subsystem.getExtensionPID(targetExtension);
      subsystem.setExtensionMotorVoltage(extensionPID);
    } else {
      extensionPID = subsystem.getExtensionPID(0);
      subsystem.setExtensionMotorVoltage(extensionPID);
    }

    isArmAtSetpoint = subsystem.isArmAtSetpoint();

  }

  @Override
  public void end(boolean interrupted) {
    subsystem.stopExtensionMotor();
    subsystem.setPitchMotorVoltage(subsystem.getPitchEncoderRadians());
  }

  @Override
  public boolean isFinished() {
    return isArmAtSetpoint;
  }

}
