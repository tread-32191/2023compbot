// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.Arm;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.filter.Debouncer;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ArmSubsystem;

public class SetArm extends CommandBase {
  /** Creates a new SetArm. */
  ArmSubsystem subsystem;
  Rotation2d angle;
  double extension;
  PIDController pitchPID, extensionPID;
  Debouncer pitchDebouncer, extensionDebouncer;
  Timer timer;
  boolean rotating;

  public SetArm(ArmSubsystem subsystem, Rotation2d angle, double extension) {
    this.subsystem = subsystem;
    this.angle = angle;
    this.extension = extension;
    addRequirements(subsystem);    

    pitchPID = new PIDController(0.8, 0, 0);
    pitchPID.setTolerance(Units.degreesToRadians(1));
    pitchDebouncer = new Debouncer(0.6);

    extensionPID = new PIDController(24, 0.5, 0);
    extensionPID.setIntegratorRange(-3, 3);
    extensionPID.setTolerance(Units.inchesToMeters(0.5));
    extensionDebouncer = new Debouncer(0.6);

    timer = new Timer();
    rotating = true;

    // Use addRequirements() here to declare subsystem dependencies.
  }
  public SetArm(ArmSubsystem subsystem, double extension) {
    this.subsystem = subsystem;
    this.angle = null;
    this.extension = extension;
    addRequirements(subsystem);    

    pitchPID = new PIDController(3.5, 0, 0.2);//0.8,0,0
    pitchPID.setTolerance(Units.degreesToRadians(1));
    pitchDebouncer = new Debouncer(0.6);

    extensionPID = new PIDController(24, 0.5, 0);
    extensionPID.setIntegratorRange(-3, 3);
    extensionPID.setTolerance(Units.inchesToMeters(0.5));
    extensionDebouncer = new Debouncer(0.6);

    timer = new Timer();
    rotating = false;

    // Use addRequirements() here to declare subsystem dependencies.
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    timer.reset();
    timer.start();
    if (rotating) {
      pitchPID.setSetpoint(angle.getRadians());
    }
    extensionPID.setSetpoint(extension);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    subsystem.setPitchMotorVoltage(
      MathUtil.clamp(
        subsystem.getPitchFFV(
            angle.getRadians(),
            pitchPID.calculate(
                subsystem.getPitchEncoderRadians())), -12, 12)

    );

    double extensionPIDCalculation = extensionPID.calculate(subsystem.getExtensionEncoderMeters());
    SmartDashboard.putNumber("extensionPIDCalculation", extensionPIDCalculation);
    extensionPIDCalculation = MathUtil.clamp(extensionPIDCalculation, -4, 4);
    subsystem.setExtensionMotorVoltage(extensionPIDCalculation);

  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    timer.stop();
    subsystem.stopAllMotors();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    boolean pitchAtSetpoint = pitchDebouncer.calculate(pitchPID.atSetpoint());
    boolean extensionAtSetpoint = extensionDebouncer.calculate(extensionPID.atSetpoint());
    return ((pitchAtSetpoint || !rotating) && extensionAtSetpoint) || timer.hasElapsed(3);
  }
}
