package frc.robot.commands.Arm;

import java.util.function.Supplier;
import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.filter.SlewRateLimiter;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants.ManipulatorConstants;
import frc.robot.Constants.OperatorPreferences;
import frc.robot.Constants.RobotPositionConstants;
import frc.robot.commands.Arm.QuickCommands.RetractArm;
import frc.robot.subsystems.ArmSubsystem;

public class MovePredictive extends CommandBase {
  ArmSubsystem subsystem;
  Supplier<Double> pitchSupplier, extensionSupplier;
  Supplier<Boolean> armFastModeSupplier, armSlowModeSupplier;
  double pitchIntention, extensionIntention,
      currentPitch, currentRelativeExtension, currentAbsoluteExtension,
      pitchVelocity, extensionVelocity,
      pitchVelocityRPS, extensionVelocityMPS,
      nextPitch, nextRelativeExtension, nextAbsoluteExtension,
      pitchOutput, extensionOutput,
      nAbsX, nAbsY, nAbsR, nAbsN,
      normError, normErrorPID;
  boolean hasNormError, fastMode, slowMode;
  SlewRateLimiter pitchLimiter, extensionLimiter;
  Translation2d currentAbsoluteVector;

  public MovePredictive(ArmSubsystem subsystem, Supplier<Double> pitchSupplier, Supplier<Double> extensionSupplier,
      Supplier<Boolean> armFastModeSupplier, Supplier<Boolean> armSlowModeSupplier) {
    this.subsystem = subsystem;
    this.pitchSupplier = pitchSupplier;
    this.extensionSupplier = extensionSupplier;
    this.armFastModeSupplier = armFastModeSupplier;
    this.armSlowModeSupplier = armSlowModeSupplier;
    addRequirements(subsystem);
    pitchLimiter = new SlewRateLimiter(OperatorPreferences.kPitchAcceleration);
    extensionLimiter = new SlewRateLimiter(OperatorPreferences.kExtensionAcceleration);
  }

  @Override
  public void initialize() {
  }

  @Override
  public void execute() {
    // Command Planning Notes:
    // - Input intention
    // - find velocity
    // - find position
    // - deploy e-fix if needed
    // - find next position
    // - find next position norm-error from subtracting maxPosNorm from NextNorm
    // - scale output buffer to correct for norm-error
    // - apply intention to output buffer (if and only if it does not conflict with
    // norm-error correction)
    // - clamp buffer to max voltage
    // - output from buffer

    // Get current input

    // get current joystick values
    pitchIntention = pitchSupplier.get();
    extensionIntention = extensionSupplier.get();
    fastMode = armFastModeSupplier.get();
    slowMode = armSlowModeSupplier.get();
    // scale inputs to speed
    pitchIntention *= OperatorPreferences.kPitchSpeedMultiplyer;
    extensionIntention *= OperatorPreferences.kExtensionSpeedMultiplyer;
    pitchIntention = (slowMode) ? pitchIntention * OperatorPreferences.kSlowSpeedMultiplyer : pitchIntention;
    extensionIntention = (slowMode) ? extensionIntention * OperatorPreferences.kSlowSpeedMultiplyer
        : extensionIntention;
    pitchIntention = (fastMode) ? pitchIntention * OperatorPreferences.kArmFastModeSpeedMultiplyer : pitchIntention;
    extensionIntention = (fastMode) ? extensionIntention * OperatorPreferences.kArmFastModeSpeedMultiplyer
        : extensionIntention;

    // ramp inputs
    pitchIntention = pitchLimiter.calculate(pitchIntention);
    extensionIntention = extensionLimiter.calculate(extensionIntention);

    // Translate the input to what the Operator Wants

    // find the intended velocities from the given inputs in radians per second and
    // meters per second respectively
    pitchVelocityRPS = pitchIntention * ManipulatorConstants.kNEOMaxFreeSpeedRPM
        * ManipulatorConstants.kPitchVelocityConversionFactor;
    extensionVelocityMPS = extensionIntention * ManipulatorConstants.kNEOMaxFreeSpeedRPM
        * ManipulatorConstants.kExtensionVelocityConversionFactor;
    // change velocities from second-based to prefered-buffer-space
    pitchVelocity = pitchVelocityRPS * ManipulatorConstants.kVelocityCheckBufferSeconds;
    extensionVelocity = extensionVelocityMPS * ManipulatorConstants.kVelocityCheckBufferSeconds;

    // Find the current Position

    // get current armPosition
    currentPitch = subsystem.getPitchEncoderRadians();
    currentRelativeExtension = subsystem.getExtensionEncoderMeters();
    // convert relative arm position to absolute arm position
    currentAbsoluteExtension = currentRelativeExtension + RobotPositionConstants.kClawMaximumLength
        + RobotPositionConstants.kPivotArmLengthFromPivotPoint;
    // create a vector to represent the arms current position;
    currentAbsoluteVector = new Translation2d(currentAbsoluteExtension, Rotation2d.fromRadians(currentPitch));

    // Deploy EMERGENCY FIX if needed

    // check to see if the current position is illegal and if so deploy EMERGENCY
    // FIX
    double absoluteVectorX = currentAbsoluteVector.getX();
    double absoluteVectorY = currentAbsoluteVector.getY();
    if (absoluteVectorX < RobotPositionConstants.kMinXFromPivot
        || absoluteVectorX > RobotPositionConstants.kMaxXFromPivot
        || absoluteVectorY < RobotPositionConstants.kMinYFromPivot
        || absoluteVectorY > RobotPositionConstants.kMaxYFromPivot) {
      new RetractArm(subsystem);
    }

    // find the next position

    // add what will happen to what has happened to get what will have happened.
    nextPitch = currentAbsoluteVector.getAngle().getRadians() + pitchVelocity;
    nextAbsoluteExtension = currentAbsoluteVector.getNorm() + extensionVelocity;
    nextRelativeExtension = nextAbsoluteExtension - RobotPositionConstants.kPivotArmLengthFromPivotPoint
        - RobotPositionConstants.kClawMaximumLength;

    // find the norm-error
    normError = nextAbsoluteExtension - subsystem.findMaxExtensionAbsolute(nextPitch);
    hasNormError = normError > 0;

    // scale output to correct for norm-error
    normErrorPID = (hasNormError) ? subsystem.getExtensionPID(subsystem.getExtensionEncoderMeters() - normError) : 0;

    // calculate buffer
    // use 12 to convert to volts
    pitchOutput = (pitchIntention * 12) + subsystem.getPitchFFV(subsystem.getPitchEncoderRadians(), pitchVelocityRPS);
    extensionOutput = (extensionIntention * 12) + normErrorPID;
    if (nextRelativeExtension > ManipulatorConstants.kMaxExtension) {
      MathUtil.clamp(extensionOutput, -12, 0);
    } else if (nextRelativeExtension < ManipulatorConstants.kMinExtension) {
      MathUtil.clamp(extensionOutput, 0, 12);
    } else {
      MathUtil.clamp(extensionOutput, -12, 12);
    }

    // apply buffer
    subsystem.setPitchMotorVoltage(pitchOutput);
    subsystem.setExtensionMotorVoltage(extensionOutput + normErrorPID);

  }

  @Override
  public void end(boolean interrupted) {
    subsystem.stopExtensionMotor();
    subsystem.setPitchMotorVoltage(subsystem.getPitchEncoderRadians());
  }

  @Override
  public boolean isFinished() {
    return false;
  }
}
