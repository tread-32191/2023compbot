package frc.robot.commands.Arm.QuickCommands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ArmSubsystem;

public class RetractArm extends CommandBase {
  ArmSubsystem subsystem;
  boolean isExtensionRetracted;

  public RetractArm(ArmSubsystem subsystem) {
    this.subsystem = subsystem;
    addRequirements(subsystem);
  }

  @Override
  public void initialize() {

  }

  @Override
  public void execute() {
    subsystem.setExtensionMotorVoltage(subsystem.getExtensionPID(0));
    isExtensionRetracted = subsystem.isExtensionAtSetpoint();
  }

  @Override
  public void end(boolean interrupted) {
    subsystem.setExtensionMotorVoltage(0);
  }

  @Override
  public boolean isFinished() {
    return isExtensionRetracted;
  }
}
