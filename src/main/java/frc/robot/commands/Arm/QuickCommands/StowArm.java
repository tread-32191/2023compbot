package frc.robot.commands.Arm.QuickCommands;

import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import frc.robot.Constants.ManipulatorConstants;
import frc.robot.commands.Arm.SetComplex;
import frc.robot.subsystems.ArmSubsystem;

public class StowArm extends InstantCommand {
  ArmSubsystem subsystem;
  SetComplex activeCommand;

  public StowArm(ArmSubsystem subsystem) {
    this.subsystem = subsystem;
  }

  @Override
  public void initialize() {
    activeCommand = new SetComplex(subsystem, Rotation2d.fromRadians(ManipulatorConstants.kPitchStartingPosition));
  }

  @Override
  public boolean isFinished() {
    return activeCommand.isFinished();
  }

  @Override
  public void end(boolean interrupted) {

  }
}
