package frc.robot.commands.Arm.QuickCommands;

import edu.wpi.first.wpilibj2.command.InstantCommand;
import frc.robot.Constants.ManipulatorConstants;
import frc.robot.Constants.RobotPositionConstants;
import frc.robot.commands.Arm.SetComplex;
import frc.robot.subsystems.ArmSubsystem;

public class SetLowScore extends InstantCommand {
  ArmSubsystem subsystem;
  SetComplex activeCommand;

  public SetLowScore(ArmSubsystem subsystem) {
    this.subsystem = subsystem;
  }

  @Override
  public void initialize() {
    activeCommand = new SetComplex(subsystem,
        ManipulatorConstants.kLowScoringPosition.getX(), ManipulatorConstants.kLowScoringPosition.getY(),
        RobotPositionConstants.kCenterOfAverageGamePieceFromExtension);
  }

  @Override
  public boolean isFinished() {
    return activeCommand.isFinished();
  }

  @Override
  public void end(boolean interrupted) {

  }
}
