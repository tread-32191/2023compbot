package frc.robot.commands.Arm.QuickCommands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants.ManipulatorConstants;
import frc.robot.Constants.RobotPositionConstants;
import frc.robot.commands.Arm.SetComplex;
import frc.robot.subsystems.ArmSubsystem;

public class SetIntake extends CommandBase {
  ArmSubsystem subsystem;
  SetComplex activeCommand;

  public SetIntake(ArmSubsystem subsystem) {
    this.subsystem = subsystem;
  }

  @Override
  public void initialize() {
    activeCommand = new SetComplex(subsystem,
        ManipulatorConstants.kFloorPosition.getX(), ManipulatorConstants.kFloorPosition.getY(),
        RobotPositionConstants.kCenterOfAverageGamePieceFromExtension);
  }

  @Override
  public boolean isFinished() {
    return activeCommand.isFinished();
  }

  @Override
  public void end(boolean interrupted) {

  }
}
