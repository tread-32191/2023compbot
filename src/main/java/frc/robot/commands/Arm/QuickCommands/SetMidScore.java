package frc.robot.commands.Arm.QuickCommands;

import edu.wpi.first.wpilibj2.command.InstantCommand;
import frc.robot.Constants.ManipulatorConstants;
import frc.robot.Constants.RobotPositionConstants;
import frc.robot.commands.Arm.SetComplex;
import frc.robot.subsystems.ArmSubsystem;

public class SetMidScore extends InstantCommand {
  ArmSubsystem subsystem;
  SetComplex activeCommand;

  public SetMidScore(ArmSubsystem subsystem) {
    this.subsystem = subsystem;
  }

  @Override
  public void initialize() {
    activeCommand = new SetComplex(subsystem,
        ManipulatorConstants.kMidScoringPosition.getX(), ManipulatorConstants.kMidScoringPosition.getY(),
        RobotPositionConstants.kCenterOfAverageGamePieceFromExtension);
  }

  @Override
  public boolean isFinished() {
    return activeCommand.isFinished();
  }

  @Override
  public void end(boolean interrupted) {

  }
}
