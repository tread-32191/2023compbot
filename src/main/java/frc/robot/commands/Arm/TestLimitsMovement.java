package frc.robot.commands.Arm;

import java.util.function.Supplier;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.filter.SlewRateLimiter;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants.ManipulatorConstants;
import frc.robot.Constants.OperatorPreferences;
import frc.robot.subsystems.ArmSubsystem;

public class TestLimitsMovement extends CommandBase {
  ArmSubsystem subsystem;
  Supplier<Double> pitchMovementSupplier, extensionMovementSupplier;
  Supplier<Boolean> armFastModeSupplier, armSlowModeSupplier;
  double intendedPitchSpeed, intendedExtensionSpeed, currentPitch, currentExtension,
      pitchInputLowerBound, pitchInputUpperBound, extensionInputLowerBound, extensionInputUpperBound;
  double extensionFuzzyLimit = Units.inchesToMeters(1);
  double pitchFuzzyLimit = Units.degreesToRadians(5);

  double extPositiveLimit = ManipulatorConstants.kMaxExtension - extensionFuzzyLimit;
  double extNegativeLimit = ManipulatorConstants.kMinExtension + extensionFuzzyLimit;
  double pitPositiveLimit = ManipulatorConstants.kMaxPitch - pitchFuzzyLimit;
  double pitNegativeLimit = ManipulatorConstants.kMinPitch + pitchFuzzyLimit;

  SlewRateLimiter pitchLimiter, extensionLimiter;

  boolean slowMode, fastMode;

  public TestLimitsMovement(ArmSubsystem subsystem, Supplier<Double> pitchMovementSupplier,
      Supplier<Double> extensionMovementSupplier, Supplier<Boolean> armFastModeSupplier,
      Supplier<Boolean> armSlowModeSupplier) {
    this.subsystem = subsystem;
    this.pitchMovementSupplier = pitchMovementSupplier;
    this.extensionMovementSupplier = extensionMovementSupplier;
    this.armFastModeSupplier = armFastModeSupplier;
    this.armSlowModeSupplier = armSlowModeSupplier;
    pitchLimiter = new SlewRateLimiter(OperatorPreferences.kPitchAcceleration);
    extensionLimiter = new SlewRateLimiter(OperatorPreferences.kExtensionAcceleration);
    addRequirements(subsystem);
  }

  @Override
  public void execute() {
    // find current position of the arm
    currentPitch = subsystem.getPitchEncoderRadians();
    currentExtension = subsystem.getExtensionEncoderMeters();// default 0 meters

    // cap the extension if it would break the arm
    extensionInputUpperBound = (currentExtension > extPositiveLimit) ? 0 : 1;
    extensionInputLowerBound = (currentExtension < extNegativeLimit) ? 0 : -1;
    // cap the extension if it would break the rules (currently needs manual
    // retraction before leaving safe zone)
    if (currentPitch < ManipulatorConstants.kReqPitchExtendMax
        || currentPitch > ManipulatorConstants.kReqPitchExtendMin) {
      SmartDashboard.putBoolean("extensionAvailable", true);
    } else {
      SmartDashboard.putBoolean("extensionAvailable", false);
      extensionInputUpperBound = 0;
    }
    // extensionInputUpperBound = (currentPitch <
    // ManipulatorConstants.kReqPitchExtendMax) ? extensionInputUpperBound : 0;
    // extensionInputUpperBound = (currentPitch >
    // ManipulatorConstants.kReqPitchExtendMin) ? extensionInputUpperBound : 0;

    // update joystick inputs
    intendedExtensionSpeed = extensionMovementSupplier.get();
    intendedPitchSpeed = pitchMovementSupplier.get();
    fastMode = armFastModeSupplier.get();
    slowMode = armSlowModeSupplier.get();

    // apply joystick deadband
    intendedExtensionSpeed = (Math.abs(intendedExtensionSpeed) < OperatorPreferences.kJoystickDeadband) ? 0
        : intendedExtensionSpeed;
    intendedPitchSpeed = (Math.abs(intendedPitchSpeed) < OperatorPreferences.kJoystickDeadband) ? 0
        : intendedPitchSpeed;

    // apply speed modifiers
    intendedExtensionSpeed *= OperatorPreferences.kExtensionSpeedMultiplyer;
    intendedPitchSpeed *= OperatorPreferences.kPitchSpeedMultiplyer;

    intendedPitchSpeed = (slowMode) ? intendedPitchSpeed * OperatorPreferences.kSlowSpeedMultiplyer
        : intendedPitchSpeed;
    intendedExtensionSpeed = (slowMode) ? intendedExtensionSpeed * OperatorPreferences.kSlowSpeedMultiplyer
        : intendedExtensionSpeed;

    intendedPitchSpeed = (fastMode) ? intendedPitchSpeed * OperatorPreferences.kArmFastModeSpeedMultiplyer
        : intendedPitchSpeed;
    intendedExtensionSpeed = (fastMode) ? intendedExtensionSpeed * OperatorPreferences.kArmFastModeSpeedMultiplyer
        : intendedExtensionSpeed;

    // apply updated clamps lastly to inputs to stop rule violations and robot
    // destruction
    intendedExtensionSpeed = MathUtil.clamp(intendedExtensionSpeed, extensionInputLowerBound, extensionInputUpperBound);
    intendedPitchSpeed = MathUtil.clamp(intendedPitchSpeed, pitchInputLowerBound, pitchInputUpperBound);

    // apply speed ramps
    intendedExtensionSpeed = extensionLimiter.calculate(intendedExtensionSpeed);
    intendedPitchSpeed = pitchLimiter.calculate(intendedPitchSpeed);

    // apply voltage to motors
    subsystem.setExtensionMotorVoltage(intendedExtensionSpeed * 12);
    subsystem.setPitchMotorVoltage(intendedPitchSpeed * 12);
  }

  @Override
  public void end(boolean interrupted) {
    subsystem.setExtensionMotorVoltage(0);
    subsystem.setPitchMotorVoltage(0);
  }

  @Override
  public boolean isFinished() {
    return false;
  }
}
