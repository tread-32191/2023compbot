package frc.robot.commands.Arm;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.filter.SlewRateLimiter;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants.ManipulatorConstants;
import frc.robot.Constants.OperatorPreferences;
import frc.robot.subsystems.ArmSubsystem;

public class SetLimited extends CommandBase {
  ArmSubsystem subsystem;

  double pitchRadiansSetpoint, extensionMetersSetpoint, pitchRadiansCurrent, extensionMetersCurrent,
      pitchPID, extensionPID,
      pitchInputLowerBound, pitchInputUpperBound, extensionInputLowerBound, extensionInputUpperBound;
  double extensionFuzzyLimit = Units.inchesToMeters(1);
  double pitchFuzzyLimit = Units.degreesToRadians(5);

  public double extPositiveLimit = ManipulatorConstants.kMaxExtension - extensionFuzzyLimit;
  public double extNegativeLimit = ManipulatorConstants.kMinExtension + extensionFuzzyLimit;
  public double pitPositiveLimit = ManipulatorConstants.kMaxPitch - pitchFuzzyLimit;
  public double pitNegativeLimit = ManipulatorConstants.kMinPitch + pitchFuzzyLimit;

  SlewRateLimiter pitchLimiter, extensionLimiter;

  boolean slowMode, fastMode;

  public SetLimited(ArmSubsystem subsystem, double pitchRadians, double extensionMeters) {
    this.subsystem = subsystem;
    this.pitchRadiansSetpoint = MathUtil.clamp(pitchRadians, ManipulatorConstants.kMinPitch,
        ManipulatorConstants.kMaxPitch);
    this.extensionMetersSetpoint = MathUtil.clamp(extensionMeters, ManipulatorConstants.kMinExtension,
        ManipulatorConstants.kMaxExtension);
    pitchLimiter = new SlewRateLimiter(OperatorPreferences.kPitchAcceleration);
    extensionLimiter = new SlewRateLimiter(OperatorPreferences.kExtensionAcceleration);
    addRequirements(subsystem);
  }

  @Override
  public void execute() {
    pitchRadiansCurrent = subsystem.getPitchEncoderRadians();// default 116 degrees in radians
    extensionMetersCurrent = subsystem.getExtensionEncoderMeters();// default 0 meters

    extensionInputUpperBound = (extensionMetersCurrent > extPositiveLimit) ? 0 : 1;
    extensionInputLowerBound = (extensionMetersCurrent < extNegativeLimit) ? 0 : -1;

    pitchInputUpperBound = (pitchRadiansCurrent > pitPositiveLimit) ? 0 : 1;
    pitchInputLowerBound = (pitchRadiansCurrent < pitNegativeLimit) ? 0 : -1;

    if (pitchRadiansCurrent < ManipulatorConstants.kReqPitchExtendMax
        || pitchRadiansCurrent > ManipulatorConstants.kReqPitchExtendMin) {
      SmartDashboard.putBoolean("extensionAvailable", true);
    } else {
      SmartDashboard.putBoolean("extensionAvailable", false);
      extensionInputUpperBound = 0;
    }

    pitchPID = subsystem.getPitchPID(pitchRadiansSetpoint);
    extensionPID = subsystem.getExtensionPID(extensionMetersSetpoint);

    extensionPID = MathUtil.clamp(extensionPID, extensionInputLowerBound, extensionInputUpperBound);
    pitchPID = MathUtil.clamp(pitchPID, pitchInputLowerBound, pitchInputUpperBound);

    subsystem.setExtensionMotorVoltage(extensionPID * 12);
    subsystem.setPitchMotorVoltage(pitchPID * 12);
  }

  @Override
  public void end(boolean interrupted) {
    subsystem.setExtensionMotorVoltage(0);
    subsystem.setPitchMotorVoltage(0);
  }

  @Override
  public boolean isFinished() {
    return false;
  }
}
