package frc.robot.commands.Arm;

import java.util.function.Supplier;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants.ManipulatorConstants;
import frc.robot.Constants.OperatorPreferences;
import frc.robot.subsystems.ArmSubsystem;

public class DIRECTDRIVE extends CommandBase {
  ArmSubsystem subsystem;
  Supplier<Double> pitchMovementSupplier, extensionMovementSupplier;

  public DIRECTDRIVE(ArmSubsystem subsystem, Supplier<Double> pitchMovementSupplier,
      Supplier<Double> extensionMovementSupplier, Supplier<Boolean> armFastModeSupplier,
      Supplier<Boolean> armSlowModeSupplier) {
    this.subsystem = subsystem;
    this.pitchMovementSupplier = pitchMovementSupplier;
    this.extensionMovementSupplier = extensionMovementSupplier;
    addRequirements(subsystem);
  }

  @Override
  public void initialize() {
  }

  @Override
  public void execute() {
    // subsystem.getExtensionEncoderMeters();
    // subsystem.getPitchEncoderRadians();

    subsystem.setPitchMotorVoltage(
        subsystem.getPitchFFV(
            subsystem.getPitchEncoderRadians(),
            pitchMovementSupplier.get() * ManipulatorConstants.kPhysicalMaxPitchSpeedRadiansPerSecond
                * OperatorPreferences.kPitchSpeedMultiplyer));
    subsystem
        .setExtensionMotorVoltage(extensionMovementSupplier.get() * 12 * OperatorPreferences.kExtensionSpeedMultiplyer);
  }

  @Override
  public void end(boolean interrupted) {
    subsystem.stopAllMotors();
  }

  @Override
  public boolean isFinished() {
    return false;
  }
}
