package frc.robot.commands.Arm.LimitSwitchCommmands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants.ManipulatorConstants;
import frc.robot.subsystems.ArmSubsystem;

public class LSPitchStow extends CommandBase {
  ArmSubsystem subsystem;

  public LSPitchStow(ArmSubsystem subsystem) {
    this.subsystem = subsystem;
    addRequirements(subsystem);
  }

  @Override
  public void initialize() {
    subsystem.setPitchMotorVoltage(ManipulatorConstants.LSPitchSpeed * 12);
  }

  @Override
  public void execute() {
  }

  @Override
  public void end(boolean interrupted) {
    subsystem.stopPitchMotor();
  }

  @Override
  public boolean isFinished() {
    return subsystem.pitchMaxLimitSupplier.getAsBoolean();
  }
}
