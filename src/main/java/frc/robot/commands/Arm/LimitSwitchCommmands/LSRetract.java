package frc.robot.commands.Arm.LimitSwitchCommmands;

import edu.wpi.first.wpilibj2.command.Commands;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.subsystems.ArmSubsystem;

public class LSRetract extends SequentialCommandGroup {
  ArmSubsystem subsystem;

  public LSRetract(ArmSubsystem subsystem) {
    this.subsystem = subsystem;
    addCommands(Commands.run(subsystem::retract, subsystem).until(subsystem.extensionMinLimitSupplier));
  }
}
