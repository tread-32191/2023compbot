package frc.robot.commands.Arm.LimitSwitchCommmands;

import edu.wpi.first.wpilibj2.command.Commands;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.subsystems.ArmSubsystem;

public class LSExtendMiddle extends SequentialCommandGroup {
  ArmSubsystem subsystem;

  public LSExtendMiddle(ArmSubsystem subsystem) {
    this.subsystem = subsystem;
    addCommands(Commands.run(subsystem::extend, subsystem).until(subsystem.extensionMidLimitSupplier));
  }
}
