package frc.robot.commands.Claw;

import edu.wpi.first.wpilibj2.command.InstantCommand;
import frc.robot.subsystems.ClawSubsystem;

public class CloseClaw extends InstantCommand {
  ClawSubsystem subsystem;

  public CloseClaw(ClawSubsystem subsystem) {
    this.subsystem = subsystem;
    addRequirements(subsystem);
  }

  @Override
  public void initialize() {
    subsystem.closeClaw();
  }
}
