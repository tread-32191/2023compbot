package frc.robot.commands.Auto;

import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.InstantCommand;

public class Stop extends InstantCommand {

  public Stop() {
  }

  @Override
  public void initialize() {
    CommandScheduler.getInstance().cancelAll();
  }
}
