package frc.robot.commands.Auto;

import java.util.ArrayList;
import java.util.List;
import com.pathplanner.lib.PathPlanner;
import com.pathplanner.lib.PathPlannerTrajectory;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.RobotContainer;
import frc.robot.UTILS.DisplayableTrajectory;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;

public class DisplayAuto {
public SendableChooser<String> startingPosition;
RobotContainer RCInterface;

public static final double defaultPathVelocity = 1.5;
public static final double defaultPathAcceleration = 3.0;

static List<PathPlannerTrajectory> blueNearSubstationList =
PathPlanner.loadPathGroup(
"blueNearSubstation", defaultPathAcceleration, defaultPathAcceleration);
static List<PathPlannerTrajectory> blueNearCenterList =
PathPlanner.loadPathGroup(
"blueNearCenter", defaultPathAcceleration, defaultPathAcceleration);
static List<PathPlannerTrajectory> blueNearJudgesList =
PathPlanner.loadPathGroup(
"blueNearJudges", defaultPathAcceleration, defaultPathAcceleration);
static List<PathPlannerTrajectory> redNearSubstationList =
PathPlanner.loadPathGroup(
"redNearSubstation", defaultPathAcceleration, defaultPathAcceleration);
static List<PathPlannerTrajectory> redNearCenterList =
PathPlanner.loadPathGroup(
"redNearCenter", defaultPathAcceleration, defaultPathAcceleration);
static List<PathPlannerTrajectory> redNearJudgesList =
PathPlanner.loadPathGroup(
"redNearJudges", defaultPathAcceleration, defaultPathAcceleration);

public static enum Autons {
blueNearSubstation(
blueNearSubstationList.get(0),
blueNearSubstationList.get(1)),
blueNearCenter(
blueNearCenterList.get(0),
blueNearCenterList.get(1)),
blueNearJudges(
blueNearJudgesList.get(0),
blueNearJudgesList.get(1)),
redNearSubstation(
redNearSubstationList.get(0),
redNearSubstationList.get(1)),
redNearCenter(
redNearCenterList.get(0),
redNearCenterList.get(1)),
redNearJudges(
redNearJudgesList.get(0),
redNearJudgesList.get(1));

public PathPlannerTrajectory leaveCommunity, balance;
public Pose2d startingPose;

Autons(PathPlannerTrajectory leaveCommunity, PathPlannerTrajectory balance) {
this.leaveCommunity = leaveCommunity;
this.balance = balance;
startingPose = leaveCommunity.getInitialHolonomicPose();
}
}

public DisplayAuto(RobotContainer RCInterface) {
this.RCInterface = RCInterface;
startingPosition = new SendableChooser<String>();
startingPosition.setDefaultOption("Near Judges", "NearJudges");
startingPosition.addOption("Near Center", "NearCenter");
startingPosition.addOption("Near Substation", "NearSubstation");
SmartDashboard.putData("startingPose", startingPosition);

SmartDashboard.putBoolean("Move After Score", true);
SmartDashboard.putBoolean("Balance After Move", false);
SmartDashboard.putBoolean("Score Mid Auto", true);
SmartDashboard.putBoolean("disableAuto", false);

}

public ArrayList<DisplayableTrajectory> getBaseTrajectories() {
ArrayList<DisplayableTrajectory> baseTrajectories = new ArrayList<>();

String basePathNames[] = { "blueNearSubstation", "blueNearCenter",
"blueNearJudges",
"redNearSubstation", "redNearCenter", "redNearJudges" };

for (int i = 0; i < basePathNames.length; i++) {
baseTrajectories.add(new DisplayableTrajectory(basePathNames[i],
Autons.valueOf(basePathNames[i]).leaveCommunity
.concatenate(Autons.valueOf(basePathNames[i]).balance)));
}

return baseTrajectories;
}

}
