package frc.robot.commands.Auto;

import java.util.List;
import java.util.function.Supplier;

import com.pathplanner.lib.PathConstraints;

import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.math.trajectory.TrajectoryConfig;
import edu.wpi.first.math.trajectory.TrajectoryGenerator;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.Commands;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.ParallelRaceGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.RobotContainer;
import frc.robot.Constants.DrivebaseConstants;
import frc.robot.commands.Arm.SetArm;
import frc.robot.commands.Auto.DisplayAuto.Autons;
import frc.robot.commands.Claw.CloseClaw;
import frc.robot.commands.Claw.OpenClaw;
import frc.robot.commands.Drive.Balance;
import frc.robot.commands.Drive.DriveBackTimed;
import frc.robot.commands.Drive.DrivebackUntilBalanceReady;
import frc.robot.commands.Drive.FlipHeadingRobot;
import frc.robot.commands.Drive.FollowTrajectory;
import frc.robot.commands.Drive.ResetOdometry;
import frc.robot.commands.Drive.SwerveBreak;
import frc.robot.commands.SmartCommands.BlindIntake;
import frc.robot.commands.SmartCommands.BlindScore;
import frc.robot.subsystems.ArmSubsystem;
import frc.robot.subsystems.Camera;
import frc.robot.subsystems.ClawSubsystem;
import frc.robot.subsystems.DrivebaseSubsystem;

public class CreateAuto {
    RobotContainer RCInterface;
    ArmSubsystem armSubsystem;
    Camera camera;
    ClawSubsystem clawSubsystem;
    DrivebaseSubsystem driveSubsystem;
    public static final Supplier<Double> emptySupplier = () -> (getZero());

    Command leaveCommunity, moveToBalance;

    String alliance, startingPosition;
    boolean isMoving, isBalancing, scoreMiddle;
    DisplayAuto displayedValues;
    Autons selectedAuton;

    public CreateAuto(
            RobotContainer RCInterface,
            DisplayAuto displayedValues,
            ArmSubsystem armSubsystem,
            Camera camera,
            ClawSubsystem clawSubsystem,
            DrivebaseSubsystem driveSubsystem) {

        this.RCInterface = RCInterface;
        this.displayedValues = displayedValues;
        this.armSubsystem = armSubsystem;
        this.camera = camera;
        this.clawSubsystem = clawSubsystem;
        this.driveSubsystem = driveSubsystem;

        switch (DriverStation.getAlliance()) {
            case Blue:
                alliance = "blue";
                break;

            case Red:
                alliance = "red";
                break;

            case Invalid:
                alliance = "blue";
                break;
        }

        startingPosition = displayedValues.startingPosition
                .getSelected();
        selectedAuton = Autons.valueOf(alliance + startingPosition);

        isMoving = SmartDashboard.getBoolean("Move After Score", true);
        isBalancing = SmartDashboard.getBoolean("Balance After Move", false);
        // Trajectory leaveCommunityTrajectory = selectedAuton.leaveCommunity;
        // isMoving = SmartDashboard.putBoolean("Move After Score", true);
        scoreMiddle = SmartDashboard.getBoolean("Score Mid Auto", true);


        // leaveCommunity = (isMoving) ? new FollowTrajectory(driveSubsystem,
        // leaveCommunityTrajectory) : new Stop();

        // RCInterface.displayPrimaryTrajectory(leaveCommunityTrajectory);
    }

    public Command getCreatedAuto() {
        if (SmartDashboard.getBoolean("disableAuto", false)) {
            return new WaitCommand(15);
        }

        Command centerMoveFirst, sidesMoveFirst;
        if (isMoving) {
            centerMoveFirst = new SequentialCommandGroup (
                new DriveBackTimed(driveSubsystem, 2.25),
                 new WaitCommand(0.1),
            new ParallelRaceGroup(
                    new FlipHeadingRobot(driveSubsystem, emptySupplier, emptySupplier),
                    new WaitCommand(1.5)),
                    new InstantCommand(driveSubsystem::zeroHeading, driveSubsystem),
                    new WaitCommand(0.1));

                    sidesMoveFirst = new SequentialCommandGroup(
                         new DriveBackTimed(driveSubsystem, 2.4),
                    new ParallelRaceGroup(
                            new FlipHeadingRobot(driveSubsystem, emptySupplier, emptySupplier),
                            new WaitCommand(1)),
                    new InstantCommand(driveSubsystem::zeroHeading, driveSubsystem));
            
            
        } else {
            centerMoveFirst = new WaitCommand(0.1);
            sidesMoveFirst = new WaitCommand(0.1);

        }

        Command balance;
        double moveBackBalanceTime = (isMoving)? 1.4 : 0.4;
        if (isBalancing) {
            balance = new SequentialCommandGroup(
                new DriveBackTimed(driveSubsystem, moveBackBalanceTime),
            new DrivebackUntilBalanceReady(driveSubsystem),
            new Balance(driveSubsystem, emptySupplier, emptySupplier, true, true));
        } else {
            balance = new WaitCommand(0.1);
        }

        Command initialSetArm = (scoreMiddle) ? new SetArm(armSubsystem, Rotation2d.fromDegrees(31),
        Units.inchesToMeters(16.5)) : new SetArm(armSubsystem, Rotation2d.fromDegrees(-20), 0);
        // Command initialSetArm = new SetArm(armSubsystem, Rotation2d.fromDegrees(-20), 0);

        // middle
        if (startingPosition == "NearCenter") {
            return new SequentialCommandGroup(
                    Commands.runOnce(() -> {
                        driveSubsystem.resetOdometry(selectedAuton.startingPose);
                    }, driveSubsystem),
                    new InstantCommand(driveSubsystem::zeroHeading, driveSubsystem),
                    new CloseClaw(clawSubsystem),
                    initialSetArm,
                    new OpenClaw(clawSubsystem),
                    new SetArm(armSubsystem, Rotation2d.fromDegrees(116), 0),
                    centerMoveFirst,
                    balance//3.0
                    // new SetArm(armSubsystem, Rotation2d.fromDegrees(-20), 0),
                    // new CloseClaw(clawSubsystem),
                    // new SetArm(armSubsystem, Rotation2d.fromDegrees(116), 0),
                    );
        } else {
            return new SequentialCommandGroup(
                    Commands.runOnce(() -> {
                        driveSubsystem.resetOdometry(selectedAuton.startingPose);
                    }, driveSubsystem),
                    new InstantCommand(driveSubsystem::zeroHeading, driveSubsystem),
                    new CloseClaw(clawSubsystem),
                    initialSetArm,
                    new OpenClaw(clawSubsystem),
                    new SetArm(armSubsystem, Rotation2d.fromDegrees(116), 0),
                   sidesMoveFirst
                    // new SetArm(armSubsystem, Rotation2d.fromDegrees(-20), 0)
                    // new CloseClaw(clawSubsystem),
                    // new SetArm(armSubsystem, Rotation2d.fromDegrees(116), 0),
                    // new DriveBackTimed(driveSubsystem, 3.0)
                    );
        }

        // Trajectory firstTrajectory = (isMoving) ? selectedAuton.leaveCommunity
        // : new Trajectory();
        // Trajectory secondTrajectory = (isMoving && isBalancing) ?
        // selectedAuton.balance
        // : new Trajectory();

        // leaveCommunity = new FollowTrajectory(driveSubsystem, firstTrajectory);
        // moveToBalance = new FollowTrajectory(driveSubsystem, secondTrajectory);

        // // RCInterface.clearBaseTrajectories();
        // //
        // RCInterface.displayPrimaryTrajectory(firstTrajectory.concatenate(secondTrajectory));

        // return new SequentialCommandGroup(
        // // new ResetOdometry(driveSubsystem, selectedAuton.startingPose),
        // new BlindScore(armSubsystem, clawSubsystem),
        // leaveCommunity,
        // // new BlindIntake(armSubsystem, clawSubsystem),
        // // moveToBalance,
        // // new Balance(driveSubsystem, emptySupplier, emptySupplier, true),
        // new SwerveBreak(driveSubsystem));

    }

    private static final double getZero() {
        return 0.0;
    }
}
