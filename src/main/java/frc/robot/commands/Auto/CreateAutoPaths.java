package frc.robot.commands.Auto;

import java.util.List;
import java.util.function.Supplier;

import com.pathplanner.lib.PathConstraints;

import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.math.trajectory.TrajectoryConfig;
import edu.wpi.first.math.trajectory.TrajectoryGenerator;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.Commands;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.ParallelRaceGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.RobotContainer;
import frc.robot.Constants.DrivebaseConstants;
import frc.robot.commands.Arm.SetArm;
import frc.robot.commands.Auto.DisplayAuto.Autons;
import frc.robot.commands.Claw.CloseClaw;
import frc.robot.commands.Claw.OpenClaw;
import frc.robot.commands.Drive.Balance;
import frc.robot.commands.Drive.DriveBackTimed;
import frc.robot.commands.Drive.DrivebackUntilBalanceReady;
import frc.robot.commands.Drive.FlipHeadingRobot;
import frc.robot.commands.Drive.FollowTrajectory;
import frc.robot.commands.Drive.ResetOdometry;
import frc.robot.commands.Drive.SwerveBreak;
import frc.robot.commands.SmartCommands.BlindIntake;
import frc.robot.commands.SmartCommands.BlindScore;
import frc.robot.subsystems.ArmSubsystem;
import frc.robot.subsystems.Camera;
import frc.robot.subsystems.ClawSubsystem;
import frc.robot.subsystems.DrivebaseSubsystem;

public class CreateAutoPaths {
    RobotContainer RCInterface;
    ArmSubsystem armSubsystem;
    Camera camera;
    ClawSubsystem clawSubsystem;
    DrivebaseSubsystem driveSubsystem;
    public static final Supplier<Double> emptySupplier = () -> (getZero());

    Command leaveCommunity, moveToBalance;

    String alliance, startingPosition;
    boolean isMoving, isBalancing, scoreMiddle;
    DisplayAuto displayedValues;
    Autons selectedAuton;

    public CreateAutoPaths(
            RobotContainer RCInterface,
            DisplayAuto displayedValues,
            ArmSubsystem armSubsystem,
            Camera camera,
            ClawSubsystem clawSubsystem,
            DrivebaseSubsystem driveSubsystem) {

        this.RCInterface = RCInterface;
        this.displayedValues = displayedValues;
        this.armSubsystem = armSubsystem;
        this.camera = camera;
        this.clawSubsystem = clawSubsystem;
        this.driveSubsystem = driveSubsystem;

        switch (DriverStation.getAlliance()) {
            case Blue:
                alliance = "blue";
                break;

            case Red:
                alliance = "red";
                break;

            case Invalid:
                alliance = "blue";
                break;
        }

        startingPosition = displayedValues.startingPosition
                .getSelected();
        selectedAuton = Autons.valueOf(alliance + startingPosition);

        isMoving = SmartDashboard.getBoolean("Move After Score", true);
        isBalancing = SmartDashboard.getBoolean("Balance After Move", false);
        // Trajectory leaveCommunityTrajectory = selectedAuton.leaveCommunity;
        isMoving = SmartDashboard.putBoolean("Move After Score", true);
        scoreMiddle = SmartDashboard.getBoolean("Score Mid Auto", true);


        // leaveCommunity = (isMoving) ? new FollowTrajectory(driveSubsystem,
        // leaveCommunityTrajectory) : new Stop();

        // RCInterface.displayPrimaryTrajectory(leaveCommunityTrajectory);
    }

    public Command getCreatedAuto() {
        Trajectory firstTrajectory = (isMoving) ? selectedAuton.leaveCommunity
                : new Trajectory();
        Trajectory secondTrajectory = (isMoving && isBalancing) ? selectedAuton.balance
                : new Trajectory();

        Command initialSetArm = (scoreMiddle) ? new SetArm(armSubsystem, Rotation2d.fromDegrees(33),
        Units.inchesToMeters(15)) : new SetArm(armSubsystem, Rotation2d.fromDegrees(-20), 0);

        leaveCommunity = new FollowTrajectory(driveSubsystem, firstTrajectory);
        moveToBalance = new FollowTrajectory(driveSubsystem, secondTrajectory);

        RCInterface.clearBaseTrajectories();

        RCInterface.displayPrimaryTrajectory(firstTrajectory.concatenate(secondTrajectory));

        return new SequentialCommandGroup(
                new CloseClaw(clawSubsystem),
                initialSetArm,
                new OpenClaw(clawSubsystem),
                new SetArm(armSubsystem, Rotation2d.fromDegrees(116), 0),
                leaveCommunity,
                new SetArm(armSubsystem, Rotation2d.fromDegrees(-20), 0),
                new CloseClaw(clawSubsystem),
                new SetArm(armSubsystem, Rotation2d.fromDegrees(116), 0),
                moveToBalance,
                new Balance(driveSubsystem, emptySupplier, emptySupplier, true, false),
                new SwerveBreak(driveSubsystem));

    }

    private static final double getZero() {
        return 0.0;
    }
}
