package frc.robot.subsystems;

import org.photonvision.PhotonCamera;
import org.photonvision.common.hardware.VisionLEDMode;
import org.photonvision.targeting.PhotonPipelineResult;
import org.photonvision.targeting.PhotonTrackedTarget;

import edu.wpi.first.math.geometry.Transform3d;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class Camera extends SubsystemBase {
    PhotonCamera camera;
    public PhotonPipelineResult result;
    public PhotonTrackedTarget bestTarget;

    public Camera() {
        camera = new PhotonCamera("Colby");
    }

    public void driverMode() {
        camera.setDriverMode(true);
        camera.setLED(VisionLEDMode.kOff);
    }

    public void detectAprilTag() {
        camera.setDriverMode(false);
        camera.setPipelineIndex(0);
        camera.setLED(VisionLEDMode.kOff);
    }

    public void detectReflectiveTape() {
        camera.setDriverMode(false);
        camera.setPipelineIndex(0);
        camera.setLED(VisionLEDMode.kOn);
    }

    public boolean update() {
        result = camera.getLatestResult();
        return result.hasTargets();
    }

    public void findBestTarget() {
        bestTarget = result.getBestTarget();
    }

    public int getAprilTagID() {
        return bestTarget.getFiducialId();
    }

    public double getSkew() {
        return bestTarget.getSkew();
    }

    public double getYaw() {
        return bestTarget.getYaw();
    }

    public double getPitch() {
        return bestTarget.getPitch();
    }

    public boolean isAmbiguosPose() {
        return bestTarget.getPoseAmbiguity() > 0.1;
    }

    public Transform3d getBestCameraToTarget() {
        return bestTarget.getBestCameraToTarget();
    }

}
