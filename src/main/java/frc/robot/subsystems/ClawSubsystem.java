package frc.robot.subsystems;

import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.ManipulatorConstants;

public class ClawSubsystem extends SubsystemBase {
  private Solenoid clawSolenoid;
  private Compressor compressor;
  public boolean state = false;

  public ClawSubsystem() {
    compressor = new Compressor(19, PneumaticsModuleType.REVPH);
    compressor.enableAnalog(100, 120);
    clawSolenoid = new Solenoid(19, PneumaticsModuleType.REVPH, ManipulatorConstants.kClawSolenoidID);
  }

  @Override
  public void periodic() {
  }

  public void toggleClaw() {
    state = !state;
    setClaw();
  }

  public void closeClaw() {
    state = true;
    setClaw();
  }

  public void openClaw() {
    state = false;
    setClaw();
  }

  private void setClaw() {
    clawSolenoid.set(state);
  }

  public boolean getState() {
    return clawSolenoid.get();
  }
}
