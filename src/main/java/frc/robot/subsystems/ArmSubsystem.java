package frc.robot.subsystems;

import java.util.function.BooleanSupplier;
import java.util.function.Supplier;
import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import frc.robot.Constants.ManipulatorConstants;
import frc.robot.Constants.RobotPositionConstants;
import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.controller.ArmFeedforward;
import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.filter.Debouncer;
import edu.wpi.first.math.filter.LinearFilter;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.DutyCycleEncoder;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class ArmSubsystem extends SubsystemBase {
  CANSparkMax extensionSparkMax, pitchSparkMax, pitchFollowerSparkMax;
  RelativeEncoder extensionRelativeEncoder, pitchRelativeEncoder;
  PIDController extensionPIDController;
  PIDController pitchPIDController;
  ArmFeedforward pitchFFCalculator;
  Debouncer extensionSetpointDebouncer, pitchSetpointDebouncer;
  ClawSubsystem clawSubsystem;
  DigitalInput pitchMaxLimit, pitchMinLimit, extensionMaxLimit, extensionMinLimit, extensionMidLimit;
  public BooleanSupplier pitchMinLimitSupplier, pitchMaxLimitSupplier, extensionMinLimitSupplier,
      extensionMaxLimitSupplier, extensionMidLimitSupplier, extensionPitchLimitSupplier;
  double pitchOutputClampMax, pitchOutputClampMin, extensionOutputClampMax, extensionOutputClampMin;
  DutyCycleEncoder pitchThroughBoreEncoder;
  // Encoder pitchThroughBoreEncoder;
  LinearFilter pitchFilter;

  public ArmSubsystem(ClawSubsystem clawSubsystem) {
    this.clawSubsystem = clawSubsystem;
    pitchFilter = LinearFilter.singlePoleIIR(0.1, 0.02);
    pitchThroughBoreEncoder = new DutyCycleEncoder(new DigitalInput(ManipulatorConstants.kThroughBoreEncoderID));
    // pitchThroughBoreEncoder.setPositionOffset(Units.degreesToRotations(116));
    pitchThroughBoreEncoder.setConnectedFrequencyThreshold(975);// rev througbore has output frequency of 975.6hz, hz
                                                                // increases with greater duration
    // pitchThroughBoreEncoder.setDistancePerRotation(Math.PI * 2);
    // pitchThroughBoreEncoder.setDutyCycleRange(0, 1);

    // pitchThroughBoreEncoder = new Encoder(0, 1);
    // pitchThroughBoreEncoder.setDistancePerPulse(Math.PI *2);

    extensionSetpointDebouncer = new Debouncer(0.1);
    pitchSetpointDebouncer = new Debouncer(0.1);

    extensionSparkMax = new CANSparkMax(ManipulatorConstants.kExtensionMotorID, MotorType.kBrushless);
    extensionSparkMax.setOpenLoopRampRate(ManipulatorConstants.kExtensionRampRate);
    extensionSparkMax.setIdleMode(IdleMode.kBrake);
    extensionSparkMax.setSmartCurrentLimit(40);
    extensionSparkMax.burnFlash();
    // extensionSparkMax.setInverted(ManipulatorConstants.kExtensionReversed);
    extensionRelativeEncoder = extensionSparkMax.getEncoder();
    // extensionRelativeEncoder.setInverted(ManipulatorConstants.kExtensionReversed);
    extensionRelativeEncoder.setPositionConversionFactor(ManipulatorConstants.kExtensionPositionConversionFactor);
    extensionRelativeEncoder.setVelocityConversionFactor(ManipulatorConstants.kExtensionVelocityConversionFactor);
    extensionRelativeEncoder.setPosition(ManipulatorConstants.kExtensionStartingPosition);
    extensionPIDController = new PIDController(ManipulatorConstants.kExtensionP, ManipulatorConstants.kExtensionI,
        ManipulatorConstants.kExtensionD);
    extensionPIDController.setTolerance(ManipulatorConstants.kExtensionTolerance);

    pitchSparkMax = new CANSparkMax(ManipulatorConstants.kPitchMotorID, MotorType.kBrushless);
    // pitchFollowerSparkMax = new
    // CANSparkMax(ManipulatorConstants.kPitchMotorFollowerID,
    // MotorType.kBrushless);
    pitchSparkMax.setOpenLoopRampRate(ManipulatorConstants.kPitchRampRate);
    pitchSparkMax.setIdleMode(IdleMode.kBrake);
    pitchSparkMax.setSmartCurrentLimit(40);
    pitchSparkMax.burnFlash();
    // pitchFollowerSparkMax.setOpenLoopRampRate(ManipulatorConstants.kPitchRampRate);
    // pitchFollowerSparkMax.setIdleMode(IdleMode.kBrake);
    // pitchFollowerSparkMax.setSmartCurrentLimit(40);
    // pitchFollowerSparkMax.burnFlash();
    // pitchFollowerSparkMax.follow(pitchSparkMax);
    pitchRelativeEncoder = pitchSparkMax.getEncoder();
    // pitchRelativeEncoder.setInverted(ManipulatorConstants.kPitchReversed);
    // pitchRelativeEncoder.setPositionConversionFactor(ManipulatorConstants.kPitchPositionConversionFactor);
    pitchRelativeEncoder.setVelocityConversionFactor(ManipulatorConstants.kPitchVelocityConversionFactor);
    pitchRelativeEncoder.setPosition(ManipulatorConstants.kPitchStartingPosition);
    pitchPIDController = new PIDController(ManipulatorConstants.kPitchP, ManipulatorConstants.kPitchI,
        ManipulatorConstants.kPitchD);
    pitchPIDController.setTolerance(ManipulatorConstants.kPitchTolerance);
    pitchFFCalculator = new ArmFeedforward(ManipulatorConstants.kPitchS, ManipulatorConstants.kPitchG,
        ManipulatorConstants.kPitchV, ManipulatorConstants.kPitchA);

    pitchMinLimit = new DigitalInput(ManipulatorConstants.kPitchMinLimiterID);
    pitchMaxLimit = new DigitalInput(ManipulatorConstants.kPitchMaxLimiterID);
    extensionMinLimit = new DigitalInput(ManipulatorConstants.kExtensionMinLimiterID);
    // extensionMidLimit = new
    // DigitalInput(ManipulatorConstants.kExtensionMidLimiterID);
    extensionMaxLimit = new DigitalInput(ManipulatorConstants.kExtensionMaxLimiterID);
    // extensionPitchLimit = new DigitalInput(ManipulatorConstants.kEPLID);

    pitchMinLimitSupplier = () -> (!pitchMinLimit.get());
    pitchMaxLimitSupplier = () -> (pitchMaxLimit.get());
    extensionMinLimitSupplier = () -> (!extensionMinLimit.get());
    // extensionMidLimitSupplier = () -> (!extensionMidLimit.get());
    extensionMaxLimitSupplier = () -> (!extensionMaxLimit.get());
    // extensionPitchLimitSupplier = () -> (pitchMaxLimit.get());
  }

  @Override
  public void periodic() {
    SmartDashboard.putNumber("armEncoder", Units.radiansToDegrees(getPitchEncoderRadians()));
  }

  public void setExtensionMotorVoltage(double outputVoltage) {
    extensionOutputClampMax = 12;
    extensionOutputClampMin = -12;

    // check to see if the extension is at a known position, and clamp for safety
    if (extensionMaxLimitSupplier.getAsBoolean()) {
      extensionOutputClampMax = 0;
      setExtensionEncoderMetersOverride(ManipulatorConstants.kMaxExtension);
    }
    // if (extensionMidLimitSupplier.get()) {
    // setExtensionEncoderMetersOverride(ManipulatorConstants.kMidExtension);
    // }
    if (extensionMinLimitSupplier.getAsBoolean()) {
      extensionOutputClampMin = 0;
      setExtensionEncoderMetersOverride(ManipulatorConstants.kMinExtension);
    }

    // ensure the arm is not extended when stowed
    if (pitchMaxLimitSupplier.getAsBoolean()) {
      extensionOutputClampMax = 0;
      // else {
      // extensionOutputClampMax = -10;
      // }
    }if (pitchMinLimitSupplier.getAsBoolean()) {
      extensionOutputClampMax = 0;
      // else {
      // extensionOutputClampMax = -10;
      // }
    }

    outputVoltage = MathUtil.clamp(outputVoltage, extensionOutputClampMin, extensionOutputClampMax);
    extensionSparkMax.setVoltage(outputVoltage);
    // SmartDashboard.putNumber("extensionVoltage", outputVoltage);
  }

  public void setPitchMotorVoltage(double outputVoltage) {
    pitchOutputClampMax = 12;
    pitchOutputClampMin = -12;

    // outputVoltage += pitchFFCalculator.calculate(getPitchEncoderRadians(), 0);

    if (pitchMaxLimitSupplier.getAsBoolean()) {
      pitchOutputClampMax = 0;
      // setPitchEncoderRadiansOverride(ManipulatorConstants.kMaxPitch);
    }

    if (pitchMinLimitSupplier.getAsBoolean()) {
      pitchOutputClampMin = 0;
      // setPitchEncoderRadiansOverride(ManipulatorConstants.kMinPitch);
    }

    outputVoltage = MathUtil.clamp(outputVoltage, pitchOutputClampMin, pitchOutputClampMax);
    pitchSparkMax.setVoltage(outputVoltage);
    // SmartDashboard.putNumber("pitchVoltage", outputVoltage);
  }

  @Deprecated
  public void setPitchMotor(double outputSpeed) {
    pitchSparkMax.set(outputSpeed);
  }

  @Deprecated
  public void setExtension(double outputSpeed) {
    extensionSparkMax.set(outputSpeed);
  }

  public void stopPitchMotor() {
    pitchSparkMax.stopMotor();
  }

  public void stopExtensionMotor() {
    extensionSparkMax.stopMotor();
  }

  public void stopAllMotors() {
    stopPitchMotor();
    stopExtensionMotor();
  }

  public void extend() {
    setExtensionMotorVoltage(5);
  }

  public void retract() {
    setExtensionMotorVoltage(-5);
  }

  public void pitchUp() {
    setPitchMotorVoltage(10);
  }

  public void pitchDown() {
    setPitchMotorVoltage(-10);
  }

  public double getPitchEncoderRadians() {
    // double pitchEncoderRadians = pitchRelativeEncoder.getPosition();
    double pitchEncoderRadians = pitchThroughBoreEncoder.getAbsolutePosition();
    pitchEncoderRadians = pitchFilter.calculate(pitchEncoderRadians);
    pitchEncoderRadians = Units.rotationsToRadians(pitchEncoderRadians);
    pitchEncoderRadians = -pitchEncoderRadians;
    pitchEncoderRadians += Units.degreesToRadians(26.5);
    pitchEncoderRadians = MathUtil.angleModulus(pitchEncoderRadians);
    
    // pitchEncoderRadians -= Units.degreesToRotations(103);
    // pitchEncoderRadians /= 200 / 2;
    // pitchEncoderRadians *= Math.PI;
    // pitchEncoderRadians /= 80;
    // pitchEncoderRadians *= ManipulatorConstants.kPitchGearRatio;
    // apply band-aid
    // pitchEncoderRadians %= Math.PI * 2;
    // pitchEncoderDegrees /= 74;
    // SmartDashboard.putNumber("Pitch Encoder Degrees",
    // Units.radiansToDegrees(pitchEncoderRadians));
    // SmartDashboard.putNumber("Pitch Encoder Degrees",
    // (Units.radiansToDegrees(pitchEncoderRadians)));
    return pitchEncoderRadians;
  }

  // private void setPitchEncoderRadiansOverride(double radians) {
  //   radians *= 200 / 2;
  //   radians /= Math.PI;
  //   pitchRelativeEncoder.setPosition(radians);
  // }

  public double getExtensionEncoderMeters() {
    double extensionEncoderMeters = extensionRelativeEncoder.getPosition();
    SmartDashboard.putNumber("Extension Encoder Inches",
    Units.metersToInches(extensionEncoderMeters));
    return extensionEncoderMeters;
  }

  private void setExtensionEncoderMetersOverride(double meters) {
    extensionRelativeEncoder.setPosition(meters);
  }

  public double getPitchFFV(double positionRadians, double velocityRadiansPerSecond) {
    return pitchFFCalculator.calculate(positionRadians, velocityRadiansPerSecond);
  }

  public double getPitchPID(double pitchRadiansFromFloor) {
    return pitchPIDController.calculate(pitchRelativeEncoder.getPosition(), pitchRadiansFromFloor);
  }

  public double getExtensionPID(double extensionMeters) {
    return extensionPIDController.calculate(extensionRelativeEncoder.getPosition(), extensionMeters);
  }

  public boolean isPitchAtSetPoint() {
    return pitchSetpointDebouncer.calculate(pitchPIDController.atSetpoint());
  }

  public boolean isExtensionAtSetpoint() {
    return extensionSetpointDebouncer.calculate(extensionPIDController.atSetpoint());
  }

  public boolean isArmAtSetpoint() {
    return isExtensionAtSetpoint() && isPitchAtSetPoint();
  }

  /**
   * Converts a given length of extension to the total length of the arm.
   * For use with checking to see if a given position is legal.
   * 
   * @param extensionLengthMeters The length (from the extension motor) to check
   * @return The total length of the arm from the pivot point
   */
  public double convertRelativeToAbsoluteExtension(double extensionLengthMeters) {
    double clawLengthMeters = (clawSubsystem.getState()) ? RobotPositionConstants.kClawMaximumLength
        : RobotPositionConstants.kClawMinimumLength;
    return extensionLengthMeters + RobotPositionConstants.kPivotArmLengthFromPivotPoint + clawLengthMeters;
  }

  /**
   * Converts a given absolute length from the pivot point to the center of the
   * claw into a given length of extension.
   * For use with finding the values to set setpoints for to go to a given target.
   * 
   * @param absoluteLengthMeters The length from the pivot point to the center of
   *                             the object
   * @return The length to extend
   */
  public double convertAbsoluteToRelativeExtensionGamePiece(double absoluteLengthMeters) {
    double extensionLengthMeters = absoluteLengthMeters - RobotPositionConstants.kPivotArmLengthFromPivotPoint;
    return extensionLengthMeters - RobotPositionConstants.kCenterOfAverageGamePieceFromExtension;
  }

  // public boolean extensionClampCheck() {
  // double secondsToBrake = 1;
  // double velocity = extensionRelativeEncoder.getVelocity();
  // double position = extensionRelativeEncoder.getPosition();
  // double upperBound = 1.5;
  // double lowerBound = 0;
  // double checkingPosition = position + (velocity * secondsToBrake);

  // return checkingPosition < upperBound && checkingPosition > lowerBound;
  // }

  public double makePitchPossible(double pitchRadians) {
    return MathUtil.clamp(pitchRadians,
        ManipulatorConstants.kMinPitch,
        ManipulatorConstants.kMaxPitch);
  }

  public double makeExtensionPossible(double relativeExtensionMeters) {
    return MathUtil.clamp(relativeExtensionMeters,
        ManipulatorConstants.kMinExtension,
        ManipulatorConstants.kMaxExtension);
  }

  /**
   * Checks if a given translation2d is within maximum extension rules
   * 
   * @param armLength Translation from the pivot point to the end of the claw
   * @return a true false statement about the validity of the position
   */
  public boolean isExtensionLegal(Translation2d maxArmTranslation) {
    double armY = maxArmTranslation.getY();
    boolean isArmYAllowed = armY < RobotPositionConstants.kMaxYFromPivot
        && armY > RobotPositionConstants.kMinYFromPivot;
    double armX = maxArmTranslation.getX();
    boolean isArmXAllowed = armX < RobotPositionConstants.kMaxXFromPivot
        && armX > RobotPositionConstants.kMinXFromPivot;
    return isArmYAllowed && isArmXAllowed;
  }

  public boolean isExtensionHelpful(double pitchRadians) {
    double unhelpfulRange = Math.PI * 0.25;
    return Math.abs(pitchRadians) < unhelpfulRange;
  }

  // max extension solver function (function accurately produces results when
  // tested)

  // make two ASA triangles with

  // A---c---B
  // | . . / |
  // b1. a . c
  // 1 / . . |
  // C2--b2--A

  // A = 90 degrees
  // B = 90 degrees-C in degrees
  // C = the functions variable

  // a = The output double (b/sin(B)) -- return the smaller result
  // b = RobotPositionConstant for max X and Y from pivot
  // c = irrelevant

  public double findMaxExtensionAbsolute(double pitchRadiansFromFloor) {
    double rightAngle = Math.PI * 0.5;

    double b1 = RobotPositionConstants.kMaxYFromPivot;
    double b2 = RobotPositionConstants.kMaxXFromPivot;
    double C1 = rightAngle - pitchRadiansFromFloor;
    double C2 = pitchRadiansFromFloor;
    double B1 = rightAngle - C1;
    double B2 = rightAngle - C2;
    double a1 = b1 / (Math.sin(B1));
    double a2 = b2 / (Math.sin(B2));

    if (a1 > a2) {
      return a2;
    } else {
      return a1;
    }
  }

}
