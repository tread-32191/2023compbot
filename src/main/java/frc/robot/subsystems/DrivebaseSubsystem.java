package frc.robot.subsystems;

import java.util.function.DoubleSupplier;
import java.util.function.Supplier;

import com.ctre.phoenix.sensors.Pigeon2;
// import com.ctre.phoenix.sensors.PigeonIMU.PigeonState;
// import com.kauailabs.navx.frc.AHRS;
import com.kauailabs.navx.frc.AHRS;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.estimator.SwerveDrivePoseEstimator;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.kinematics.SwerveDriveKinematics;
import edu.wpi.first.math.kinematics.SwerveDriveOdometry;
import edu.wpi.first.math.kinematics.SwerveModulePosition;
import edu.wpi.first.math.kinematics.SwerveModuleState;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.util.sendable.Sendable;
import edu.wpi.first.util.sendable.SendableBuilder;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.ModuleObject;
import frc.robot.RobotContainer;
import frc.robot.Constants.DrivebaseConstants;
import frc.robot.Constants.MovementConstants;
import frc.robot.commands.Drive.ZeroHeading;

public class DrivebaseSubsystem extends SubsystemBase {
  private final ModuleObject frontLeft = new ModuleObject(
      DrivebaseConstants.kFrontLeftDriveMotorPort,
      DrivebaseConstants.kFrontLeftTurningMotorPort,
      DrivebaseConstants.kFrontLeftDriveReversed,
      DrivebaseConstants.kFrontLeftTurningReversed,
      DrivebaseConstants.kFrontLeftDriveAbsoluteEncoderPort,
      DrivebaseConstants.kFrontLeftDriveAbsoluteEncoderOffsetRad,
      DrivebaseConstants.kFrontLeftDriveAbsoluteEncoderReversed,
      DrivebaseConstants.kFrontLeftDriveChannelID);

  private final ModuleObject frontRight = new ModuleObject(
      DrivebaseConstants.kFrontRightDriveMotorPort,
      DrivebaseConstants.kFrontRightTurningMotorPort,
      DrivebaseConstants.kFrontRightDriveReversed,
      DrivebaseConstants.kFrontRightTurningReversed,
      DrivebaseConstants.kFrontRightDriveAbsoluteEncoderPort,
      DrivebaseConstants.kFrontRightDriveAbsoluteEncoderOffsetRad,
      DrivebaseConstants.kFrontRightDriveAbsoluteEncoderReversed,
      DrivebaseConstants.kFrontRightDriveChannelID);

  private final ModuleObject backLeft = new ModuleObject(
      DrivebaseConstants.kBackLeftDriveMotorPort,
      DrivebaseConstants.kBackLeftTurningMotorPort,
      DrivebaseConstants.kBackLeftDriveReversed,
      DrivebaseConstants.kBackLeftTurningReversed,
      DrivebaseConstants.kBackLeftDriveAbsoluteEncoderPort,
      DrivebaseConstants.kBackLeftDriveAbsoluteEncoderOffsetRad,
      DrivebaseConstants.kBackLeftDriveAbsoluteEncoderReversed,
      DrivebaseConstants.kBackLeftDriveChannelID);

  private final ModuleObject backRight = new ModuleObject(
      DrivebaseConstants.kBackRightDriveMotorPort,
      DrivebaseConstants.kBackRightTurningMotorPort,
      DrivebaseConstants.kBackRightDriveReversed,
      DrivebaseConstants.kBackRightTurningReversed,
      DrivebaseConstants.kBackRightDriveAbsoluteEncoderPort,
      DrivebaseConstants.kBackRightDriveAbsoluteEncoderOffsetRad,
      DrivebaseConstants.kBackRightDriveAbsoluteEncoderReversed,
      DrivebaseConstants.kBackRightDriveChannelID);

  // private final AHRS navx;
  private final Pigeon2 pigeon;
  double headingOffset = 0;
  Rotation2d yawOffset = Rotation2d.fromDegrees(DrivebaseConstants.kYawOffsetDegrees);
  RobotContainer RCInterface;

  // private final SwerveDriveOdometry odometry = new SwerveDriveOdometry(DrivebaseConstants.kDriveKinematics,
  //     yawOffset, getModulePositons());

  public final SwerveDrivePoseEstimator poseEstimator = new SwerveDrivePoseEstimator(
      DrivebaseConstants.kDriveKinematics, new Rotation2d(), getModulePositons(), new Pose2d(0, 0, Rotation2d.fromDegrees(0)));

  // private final SwerveDrivePoseEstimator poseEstimator = new
  // SwerveDrivePoseEstimator(
  // DrivebaseConstants.kDriveKinematics, yawOffset, getModulePositons(), new
  // Pose2d());

  public DrivebaseSubsystem(RobotContainer RCInterface) {
    this.RCInterface = RCInterface;
    // navx = new AHRS(SPI.Port.kMXP);
    pigeon = new Pigeon2(32);
    pigeon.clearStickyFaults();
    pigeon.configFactoryDefault();

    new Thread(() -> {
      try {
        Thread.sleep(1000);
        // navx.calibrate();
        zeroEncoders();
        
        pigeon.zeroGyroBiasNow();
      } catch (Exception e) {
      }
    }).start();
  }

  public void setPose() {
    zeroEncoders();
    // poseEstimator.resetPosition(getYaw(), getModulePositons(), new Pose2d());
  }

  public void zeroHeading() {
    // navx.zeroYaw();
    pigeon.setYaw(0);
  }

  public Rotation2d getYaw() {
    // Rotation2d yawReading = Rotation2d.fromRadians(
    //     -MathUtil.angleModulus(
    //         Units.degreesToRadians(
    //             navx.getYaw())));
    Rotation2d yawReading = Rotation2d.fromRadians(
    -MathUtil.angleModulus(
    Units.degreesToRadians(
    pigeon.getYaw())));

    SmartDashboard.putNumber("yaw", yawReading.getDegrees());
    return yawReading;
  }

  public Rotation2d getRoll() {
    // return Rotation2d.fromDegrees(navx.getRoll());
    return Rotation2d.fromDegrees(pigeon.getRoll());
  }

  public Rotation2d getPitch() {
    // return Rotation2d.fromDegrees(navx.getPitch());
    return Rotation2d.fromDegrees(pigeon.getPitch());
  }

  public Pose2d getPose() {
    // return odometry.getPoseMeters();
    return poseEstimator.getEstimatedPosition();
  }

  public void resetOdometry(Pose2d pose) {
    poseEstimator.resetPosition(getYaw(), getModulePositons(), pose);
    // odometry.resetPosition(getYaw(), getModulePositons(), pose);
  }

  @Override
  public void periodic() {
    poseEstimator.update(getYaw(), getModulePositons());
    // odometry.update(getYaw(), getModulePositons());
    RCInterface.displayRobotPose(getPose());
    // // SmartDashboard.putNumber("Robot Heading", getYaw().getDegrees());
    // // SmartDashboard.putString("Robot Location",
    // getPose().getTranslation().toString());
    // // SmartDashboard.putNumber("frontLeftTurning",
    // frontLeft.getTurningPosition());
    // // SmartDashboard.putNumber("frontRightTurning",
    // // frontRight.getTurningPosition());
    // // SmartDashboard.putNumber("backLeftTurning",
    // backLeft.getTurningPosition());
    // // SmartDashboard.putNumber("backRightTurning",
    // backRight.getTurningPosition());
    // // SmartDashboard.putNumber("Front Left Abs Encoder",
    // Units.radiansToDegrees(frontLeft.getAbsoluteEncoderRad()));
    // // SmartDashboard.putNumber("Front Right Abs Encoder",
    // Units.radiansToDegrees(frontRight.getAbsoluteEncoderRad()));
    // // SmartDashboard.putNumber("Back Left Abs Encoder",
    // Units.radiansToDegrees(backLeft.getAbsoluteEncoderRad()));
    // // SmartDashboard.putNumber("Back Right Abs Encoder",
    // Units.radiansToDegrees(backRight.getAbsoluteEncoderRad()));

  }

  public SwerveModulePosition[] getModulePositons() {
    SwerveModulePosition[] modulePositions = { frontLeft.getPosition(), frontRight.getPosition(),
        backLeft.getPosition(), backRight.getPosition() };
    return modulePositions;
  }

  public double[] getAbsoluteEncoderRads() {
    double[] absoluteEncoderRads = {
        frontLeft.getAbsoluteEncoderRad(),
        frontRight.getAbsoluteEncoderRad(),
        backLeft.getAbsoluteEncoderRad(),
        backRight.getAbsoluteEncoderRad() };
    return absoluteEncoderRads;
  }

  public void stopModules() {
    frontLeft.stop();
    frontRight.stop();
    backLeft.stop();
    backRight.stop();
  }

  public void setModuleStates(SwerveModuleState[] desiredStates) {
    SwerveDriveKinematics.desaturateWheelSpeeds(desiredStates, MovementConstants.kPhysicalMaxSpeedMPS);
    frontLeft.setDesiredState(desiredStates[0]);
    frontRight.setDesiredState(desiredStates[1]);
    backLeft.setDesiredState(desiredStates[2]);
    backRight.setDesiredState(desiredStates[3]);
    SmartDashboard.putNumber("Front Left Abs Encoder", Units.radiansToDegrees(frontLeft.getAbsoluteEncoderRad()));
    SmartDashboard.putNumber("Front Right Abs Encoder", Units.radiansToDegrees(frontRight.getAbsoluteEncoderRad()));
    SmartDashboard.putNumber("Back Left Abs Encoder", Units.radiansToDegrees(backLeft.getAbsoluteEncoderRad()));
    SmartDashboard.putNumber("Back Right Abs Encoder", Units.radiansToDegrees(backRight.getAbsoluteEncoderRad()));
  }

  public void zeroWheelDirection() {
    frontLeft.setDesiredState(new SwerveModuleState(1, Rotation2d.fromDegrees(0)));
    frontRight.setDesiredState(new SwerveModuleState(1, Rotation2d.fromDegrees(0)));
    backLeft.setDesiredState(new SwerveModuleState(1, Rotation2d.fromDegrees(0)));
    backRight.setDesiredState(new SwerveModuleState(1, Rotation2d.fromDegrees(0)));
  }

  public void setForceBreak() {
    frontLeft.setForceBreak();
    frontRight.setForceBreak();
    backLeft.setForceBreak();
    backRight.setForceBreak();
  }

  public boolean modulesAtTurningSetpoint() {
    return (frontLeft.atSetpoint() &&
        frontRight.atSetpoint() &&
        backLeft.atSetpoint() &&
        backRight.atSetpoint());
  }

  public void setDriveIdleBrakeMode(boolean brake) {
    frontLeft.setIdleBrake(brake);
    frontRight.setIdleBrake(brake);
    backLeft.setIdleBrake(brake);
    backRight.setIdleBrake(brake);
  }

  public void zeroEncoders() {
    frontLeft.resetEncoders();
    frontRight.resetEncoders();
    backLeft.resetEncoders();
    backRight.resetEncoders();
  }

}
