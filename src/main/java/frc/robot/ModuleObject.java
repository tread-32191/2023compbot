package frc.robot;

import com.revrobotics.RelativeEncoder;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.controller.SimpleMotorFeedforward;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.kinematics.SwerveModulePosition;
import edu.wpi.first.math.kinematics.SwerveModuleState;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.DutyCycleEncoder;
import edu.wpi.first.wpilibj.RobotController;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Constants.DrivebaseConstants;
import frc.robot.Constants.ManipulatorConstants;
import frc.robot.Constants.ModuleConstants;
import frc.robot.Constants.MovementConstants;

public class ModuleObject {
  public DutyCycleEncoder encoder;
  private boolean absoluteEncoderReversed;
  private SimpleMotorFeedforward driveFeedForward, turnFeedForward;

  private final CANSparkMax driveMotor;
  private final CANSparkMax turningMotor;

  private final RelativeEncoder driveEncoder;
  private final RelativeEncoder turningEncoder;
  private final AnalogInput absEncoder;

  private final PIDController turningPidController;
  double startTimeAngle = 0;
  double absoluteEncoderOffsetRad;

  private int channelID;

  // LinearFilter moduleFilter = LinearFilter.movingAverage(10);

  public ModuleObject(int driveMotorId, int turningMotorId, boolean driveMotorReversed, boolean turningMotorReversed,
      int absoluteEncoderID, double absoluteEncoderOffsetRad, boolean absoluteEncoderReversed,
      int channelID) {

    this.absoluteEncoderOffsetRad = absoluteEncoderOffsetRad;
    this.absoluteEncoderReversed = absoluteEncoderReversed;
    absEncoder = new AnalogInput(absoluteEncoderID);

    driveMotor = new CANSparkMax(driveMotorId, MotorType.kBrushless);
    driveMotor.setInverted(driveMotorReversed);
    setIdleBrake(true);
    driveMotor.setSmartCurrentLimit(40);

    turningMotor = new CANSparkMax(turningMotorId, MotorType.kBrushless);
    turningMotor.setInverted(turningMotorReversed);
    turningMotor.setIdleMode(IdleMode.kBrake);
    turningMotor.setSmartCurrentLimit(40);

    driveEncoder = driveMotor.getEncoder();
    driveEncoder.setPositionConversionFactor(ModuleConstants.kDriveEncoderRot2Meter);
    driveEncoder.setVelocityConversionFactor(ModuleConstants.kDriveEncoderRPM2MeterPerSec);

    turningEncoder = turningMotor.getEncoder();
    turningEncoder.setPositionConversionFactor(ModuleConstants.kTurningEncoderRot2Rad);
    turningEncoder.setVelocityConversionFactor(ModuleConstants.kTurningEncoderRPM2RadPerSec);

    turningPidController = new PIDController(ModuleConstants.kTurnP, ModuleConstants.kTurnI, ModuleConstants.kTurnD);
    turningPidController.enableContinuousInput(-Math.PI, Math.PI);

    driveFeedForward = new SimpleMotorFeedforward(
        ModuleConstants.kDriveS,
        ModuleConstants.kDriveV,
        ModuleConstants.kDriveA);

    turnFeedForward = new SimpleMotorFeedforward(
        ModuleConstants.kTurnS,
        ModuleConstants.kTurnV,
        ModuleConstants.kTurnA);

    this.channelID = channelID;
  }

  public double getDrivePosition() {
    double position = driveEncoder.getPosition();
    // SmartDashboard.putNumber("testPositionDrive" + channelID, position);
    return position;

  }

  public double getTurningPosition() {
    double positon = turningEncoder.getPosition();
    // SmartDashboard.putNumber("testPositionTurning" + channelID, positon);
    return positon;
    // return turningEncoder.getPosition();
  }

  public double getDriveVelocity() {
    return driveEncoder.getVelocity();
  }

  public double getTurningVelocity() {
    return turningEncoder.getVelocity();
  }

  // public double getAbsoluteEncoderRad() {
  // startTimeAngle = encoder.getAbsolutePosition() - encoder.getPositionOffset();
  // startTimeAngle = (absoluteEncoderReversed) ? startTimeAngle * -1 :
  // startTimeAngle;
  // return Units.rotationsToRadians(startTimeAngle);
  // }

  public double getAbsoluteEncoderRad() {
    double angle = absEncoder.getVoltage() /
        RobotController.getVoltage5V();
    // angle = moduleFilter.calculate(angle);
    angle *= Math.PI * 2;
    angle -= absoluteEncoderOffsetRad;
    angle %= Math.PI * 2;
    return angle * (absoluteEncoderReversed ? -1.0 : 1.0);
  }

  public void resetEncoders() {
    driveEncoder.setPosition(0);
    turningEncoder.setPosition(getAbsoluteEncoderRad());
  }

  public SwerveModuleState getState() {
    return new SwerveModuleState(getDriveVelocity(), new Rotation2d(getTurningPosition()));
  }

  public SwerveModulePosition getPosition() {
    return new SwerveModulePosition(getDrivePosition(), Rotation2d.fromRadians(getTurningPosition()));
  }

  public void setDesiredState(SwerveModuleState state) {
    if (Math.abs(state.speedMetersPerSecond) < 0.001) {
      stop();
      return;
    }
    state = SwerveModuleState.optimize(state, getState().angle);

    double turnVolts = turnFeedForward.calculate(
        turningPidController.calculate(
            getTurningPosition(), state.angle.getRadians()) * ModuleConstants.kPhysicalTurnMaxRadiansPerSecond);
    // double turnVolts = turningPidController.calculate(
    //   getTurningPosition(), state.angle.getRadians()) *12;

    turnVolts = MathUtil.clamp(turnVolts, -12, 12);

    double driveVolts = driveFeedForward.calculate(
        state.speedMetersPerSecond);

        // double driveVolts = state.speedMetersPerSecond / MovementConstants.kPhysicalMaxSpeedMPS * 12;
    driveVolts = MathUtil.clamp(driveVolts, -12, 12);

    turningMotor.setVoltage(turnVolts);
    driveMotor.setVoltage(driveVolts);
    SmartDashboard.putNumber("Swerve [" + channelID + "] current Amps", driveMotor.getOutputCurrent());
  }

  public void setForceBreak() {
    double angle = (channelID == DrivebaseConstants.kFrontLeftDriveChannelID
        || channelID == DrivebaseConstants.kBackRightDriveChannelID) ? Units.degreesToRadians(-42)
            : Units.degreesToRadians(42);
    turningMotor.set(turningPidController.calculate(getTurningPosition(), angle));
    driveMotor.set(0);
  }

  public void setIdleBrake(boolean brake) {
    driveMotor.setIdleMode((brake) ? IdleMode.kBrake : IdleMode.kCoast);
  }

  public void stop() {
    driveMotor.set(0);
    turningMotor.set(0);
  }

  public boolean atSetpoint() {
    return turningPidController.atSetpoint();
  }

}
