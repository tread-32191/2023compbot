package frc.robot;

import java.util.List;
import com.pathplanner.lib.PathConstraints;
import com.pathplanner.lib.PathPlanner;
import com.pathplanner.lib.PathPlannerTrajectory;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Pose3d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Rotation3d;
import edu.wpi.first.math.geometry.Transform3d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.geometry.Translation3d;
import edu.wpi.first.math.kinematics.SwerveDriveKinematics;
import edu.wpi.first.math.trajectory.TrapezoidProfile;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.Preferences;
import edu.wpi.first.wpilibj.DriverStation.Alliance;

public final class Constants {

  public static final char STEVE = '\uf600';
  // File Overview: (Ordered by neccesity and possibility for variability)
  // ------------------------------------
  // - OperatorPreferences
  // -- All preferences likely to be changed depending on driver(s) and
  // controllers

  // - VisionConstants
  // -- All constants about the camera(s), pipelines, and optical sensors

  // - MovementConstants
  // -- All constants about pathing, driving, balancing, and auto

  // - ManipulatorConstants
  // -- All constants to do with the arm and hand

  // - DrivebaseConstants
  // -- All constants to do with the drivebase

  // - ModuleConstants
  // -- All constants that apply to each individual swerve module

  // - RobotPositionConstants
  // -- Robot-relative positions

  // - FieldPositionConstants
  // -- Field-relative positions

  // Motor IDs:
  // ------------------------------------
  // 1:Front Right Module Turning
  // 2:Front Right Module Driving
  // 3:Back Left Module Turning
  // 4:Back Left Module Driving
  // 5:Back Right Module Turning
  // 6:Front Left Module Driving
  // 7:Back Right Module Driving
  // 8:Front Left Module Turning
  // 9:Arm Pivoting
  // 10: Arm Extension

  // Solenoid IDs:
  // ------------------------------------
  // 0:Claw

  public static final class OperatorPreferences {
    public static final int kDriverControllerPort = 0;
    public static final int kGunnerControllerPort = 1;

    public static final double kTriggerDeadband = 0.05;
    public static final double kJoystickDeadband = 0.1;

    // public static final double kDriveMovementSpeedMultiplyer = 0.4;
    // public static final double kDriveTurnSpeedMultiplyer = 0.25;

    public static final double kPitchSpeedMultiplyer = 1.0;
    public static final double kExtensionSpeedMultiplyer = 1.0;
    public static final double kArmFastModeSpeedMultiplyer = 1.5;
    public static final double kSlowSpeedMultiplyer = 0.5;

    public static final double kPitchAcceleration = 0.5;
    public static final double kExtensionAcceleration = 0.5;
    public static final double kAccelerationMPS = 1.75;
    public static final double kAccelerationRPS = 1.75;
    public static final double kTeleOPSpeedMultiplyer = 0.25;
    public static final double kSlowModeSpeedMultiplyer = 0.125;
    public static final double kFastModeSpeedMultiplyer = 0.75;
  }

  public static final class VisionConstants {
    public static final String kCameraName = "cameraName";
    public static final double kStoppingDistanceAprilTagMeters = 1.0;
  }

  public static final class MovementConstants {
    public static final double kAccelerationMPS = 1.75;
    public static final double kAccelerationRPS = 1.75;

    public static final double kPhysicalMaxSpeedMPS = 3.755127;
    public static final double kPhysicalMaxSpeedRPS = 2 * (2 * Math.PI);
    // public static final double kPhysicalMaxAccelerationMPS = 3.5;
    // public static final double kPhysicalMaxAccelerationRPS = 3.5;

    public static final double kTeleOPSpeedMultiplyer = 0.25;
    public static final double kTeleOpMaxSpeedMPS = kPhysicalMaxSpeedMPS * kTeleOPSpeedMultiplyer;
    public static final double kTeleOpMaxSpeedRPS = kPhysicalMaxSpeedRPS * kTeleOPSpeedMultiplyer;
    // public static final double kTeleOpMaxAccelerationMPS = 1.5;
    // public static final double kTeleOpMaxAccelerationRPS = 1.5;

    public static final double kSlowModeSpeedMultiplyer = 0.125;
    public static final double kSlowModeSpeedMPS = kPhysicalMaxSpeedRPS * kSlowModeSpeedMultiplyer;
    public static final double kSlowModeSpeedRPS = kPhysicalMaxSpeedRPS * kSlowModeSpeedMultiplyer;
    // public static final double kSlowModeAccelerationMPS = 0.8;
    // public static final double kSlowModeAccelerationRPS = 0.8;

    public static final double kFastModeSpeedMultiplyer = 0.75;
    public static final double kFastModeSpeedMPS = kPhysicalMaxSpeedMPS * kFastModeSpeedMultiplyer;
    public static final double kFastModeSpeedRPS = kPhysicalMaxSpeedRPS * kFastModeSpeedMultiplyer;
    // public static final double kFastModeAccelerationMPS = 2.5;
    // public static final double kFastModeAccelerationRPS = 2.5;

    public static final double kPathingMaxSpeedMPS = kPhysicalMaxSpeedMPS * 0.25;
    public static final double kPathingMaxSpeedRPS = kPhysicalMaxSpeedRPS * 0.1;
    public static final double kPathingMaxAccelerationMPS = 3.0;
    public static final double kPathingMaxAccelerationRPS = Math.PI * 0.25;

    public static final double kPathingXP = 1.5;
    public static final double kPathingYP = 1.5;
    public static final double kPathingThetaP = 3;

    public static final PathConstraints kPathPlannerConstraints = new PathConstraints(kPathingMaxSpeedMPS,
        kPathingMaxAccelerationMPS);
    public static final TrapezoidProfile.Constraints kPathingThetaConstraints = new TrapezoidProfile.Constraints(
        kPathingMaxSpeedRPS, kPathingMaxAccelerationRPS);

    public static final String paths[] = {
        "red1Start-lowerMostCargo",
        "red2Start-bottomMiddleCargo", "red2Start-topMiddleCargo",
        "red3Start-upperMostCargo",
        "blue1Start-upperMostCargo",
        "blue2Start-topMiddleCargo", "blue2Start-bottomMiddleCargo",
        "blue3Start-lowerMostCargo" };

    public static final double kPhysicalMaxAngularSpeedRadiansPerSecond = 2 * 2 * Math.PI;
    public static final double kMaxTiltRadians = Units.degreesToRadians(15);
    public static final double kTiltTolerance = Units.degreesToRadians(3);
    public static final double kBalanceP = 0.045;
    public static final double kBalanceI = 0.0005;
    public static final double kBalanceD = 0.0075;
    public static final double kConvertTiltToInput = 1.0 / kMaxTiltRadians;
    public static final double kTimeToSwerveBreak = 0.25;
    public static final double kTimeToBalance = 1.25;

  }

  public static final class ManipulatorConstants {
    public static final double LSExtensionSpeed = 0.3;
    public static final double LSPitchSpeed = 0.5;

    public static final int kPitchMotorID = 10;
    public static final int kPitchMotorFollowerID = 11;
    public static final int kExtensionMotorID = 9;
    public static final int kClawSolenoidID = 0;

    public static final int kThroughBoreEncoderID = 1;

    public static final int kPitchMaxLimiterID = 4;
    public static final int kPitchMinLimiterID = 5;
    public static final int kExtensionMaxLimiterID = 3;
    public static final int kExtensionMidLimiterID = 0;
    public static final int kExtensionMinLimiterID = 2;
    // public static final int kEPLID = 1;

    // public static final boolean kStartingClawState = false;

    public static final double kNEOMaxFreeSpeedRPM = 5760;
    public static final double kVelocityCheckBufferSeconds = 0.25;

    public static final double kPitchMotorGearboxRatio = 80.0 / 1.0;
    // public static final double kPitchMotorGearboxRatio = 1.0/80.0;
    public static final double kPitchMotorSprocketRatio = 60.0 / 12.0;
    // public static final double kPitchMotorSprocketRatio = 12.0/60.0;
    public static final double kPitchGearRatio = kPitchMotorGearboxRatio * kPitchMotorSprocketRatio;
    public static final double kRotationsToRadians = 2 * Math.PI;
    // public static final double kPitchPositionConversionFactor = kPitchGearRatio *
    // kRotationsToRadians;
    // public static final double kPitchPositionConversionFactor = kPitchGearRatio;

    public static final double kPhysicalMaxPitchSpeedRadiansPerSecond = (96 * kRotationsToRadians) / 400;

    public static final double kPitchPositionConversionFactor = kRotationsToRadians * kPitchGearRatio;
    public static final double kTemp = kPitchPositionConversionFactor * 5760;
    public static final double kPitchVelocityConversionFactor = kPitchPositionConversionFactor / 60.0;

    public static final double kExtensionSprocketDiameter = Units.inchesToMeters(1.0);
    public static final double kExtensionSprocketCircumference = kExtensionSprocketDiameter * Math.PI;
    public static final double kExtensionGearboxRatio = 1.0 / 12.0;
    public static final double kExtensionRotationsToMeters = kExtensionGearboxRatio * kExtensionSprocketCircumference;
    public static final double kExtensionPositionConversionFactor = kExtensionRotationsToMeters;
    public static final double kExtensionVelocityConversionFactor = kExtensionPositionConversionFactor / 60.0;

    public static final double kPitchP = 0.01379310345;// ---<Unfinished
    public static final double kPitchI = 0.0;// ---<Unfinished
    public static final double kPitchD = 0.0;// ---<Unfinished
    public static final double kPitchS = 0.22885;
    public static final double kPitchG = 0.12;
    public static final double kPitchV = 7.3104;
    public static final double kPitchA = 0.69243;
    public static final double kPitchManualAcceleration = 1.0;

    public static final double kExtensionP = 0.127388535;// ---<Unfinished
    public static final double kExtensionI = 0.0;// ---<Unfinished
    public static final double kExtensionD = 0.0;// ---<Unfinished
    // public static final double kExtensionS = 0.0;// ---<Unfinished
    // public static final double kExtensionG = 0.05;
    // public static final double kExtensionV = 3.07;
    // public static final double kExtensionA = 0.01;
    public static final double kExtensionManualAcceleration = 1.0;

    public static final double kPitchTolerance = Units.degreesToRadians(3);
    public static final double kExtensionTolerance = Units.inchesToMeters(2);

    public static final double kReqPitchExtendToleranceDeg = 5.0;
    public static final double kReqPitchExtendMin = Units.degreesToRadians(5.0 + kReqPitchExtendToleranceDeg);
    public static final double kReqPitchExtendMax = Units.degreesToRadians(62.0 - kReqPitchExtendToleranceDeg);

    // Radians
    public static final double kPitchStartingPosition = Units.degreesToRadians(116);
    public static final double kMaxPitch = kPitchStartingPosition;
    public static final double kMinPitch = Units.degreesToRadians(-20);

    public static final double kExtensionStartingPosition = 0.0;
    public static final double kMaxExtension = RobotPositionConstants.kMaximumExtension;
    public static final double kMidExtension = Units.inchesToMeters(14);
    public static final double kMinExtension = kExtensionStartingPosition;

    public static final boolean kPitchReversed = true;
    public static final boolean kExtensionReversed = true;

    public static final double kBeforeDropPitchToScore = Units.degreesToRadians(31.5);
    public static final double kActualPitchToScore = Units.degreesToRadians(28.5);
    public static final double kFloorPitch = Units.degreesToRadians(-20);
    public static final double kSubstationPitch = Units.degreesToRadians(37.5);

    public static final Translation2d kHighScoringPosition = new Translation2d(kMaxExtension,
        Rotation2d.fromRadians(kBeforeDropPitchToScore));
    public static final Translation2d kMidScoringPosition = new Translation2d(kMinExtension,
        Rotation2d.fromRadians(kBeforeDropPitchToScore));
    public static final Translation2d kLowScoringPosition = new Translation2d(kMinExtension,
        Rotation2d.fromRadians(kFloorPitch));
    public static final Translation2d kFloorPosition = new Translation2d(kMinExtension,
        Rotation2d.fromRadians(kFloorPitch));
    public static final Translation2d kSubstationPosition = new Translation2d(Units.inchesToMeters(13.5),
        Rotation2d.fromRadians(kSubstationPitch));

    public static final double kExtensionRampRate = 1;
    public static final double kPitchRampRate = 2;
  }

  public static final class DrivebaseConstants {
    public static final double kYawOffsetDegrees = 0;

    public static final SwerveDriveKinematics kDriveKinematics = new SwerveDriveKinematics(
        RobotPositionConstants.kFrontLeftModuleCenter,
        RobotPositionConstants.kFrontRightModuleCenter,
        RobotPositionConstants.kBackLeftModuleCenter,
        RobotPositionConstants.kBackRightModuleCenter);

    public static final int kFrontRightDriveMotorPort = 2;
    public static final int kFrontLeftDriveMotorPort = 6;
    public static final int kBackLeftDriveMotorPort = 4;
    public static final int kBackRightDriveMotorPort = 7;

    public static final int kFrontRightTurningMotorPort = 1;
    public static final int kFrontLeftTurningMotorPort = 8;
    public static final int kBackLeftTurningMotorPort = 3;
    public static final int kBackRightTurningMotorPort = 5;

    public static final boolean kFrontLeftTurningReversed = true;
    public static final boolean kBackLeftTurningReversed = true;
    public static final boolean kFrontRightTurningReversed = true;
    public static final boolean kBackRightTurningReversed = true;

    public static final boolean kFrontLeftDriveReversed = false;
    public static final boolean kBackLeftDriveReversed = false;
    public static final boolean kFrontRightDriveReversed = false;
    public static final boolean kBackRightDriveReversed = false;

    public static final int kFrontLeftDriveChannelID = 1;
    public static final int kFrontRightDriveChannelID = 2;
    public static final int kBackLeftDriveChannelID = 3;
    public static final int kBackRightDriveChannelID = 4;

    public static final int kFrontLeftDriveAbsoluteEncoderPort = 3;
    public static final int kFrontRightDriveAbsoluteEncoderPort = 2;
    public static final int kBackLeftDriveAbsoluteEncoderPort = 1;
    public static final int kBackRightDriveAbsoluteEncoderPort = 0;

    public static final boolean kFrontLeftDriveAbsoluteEncoderReversed = true;// CCW
    public static final boolean kBackLeftDriveAbsoluteEncoderReversed = true;// CCW
    public static final boolean kFrontRightDriveAbsoluteEncoderReversed = true;// CCW
    public static final boolean kBackRightDriveAbsoluteEncoderReversed = true;// CCw
    public static final double kFrontLeftDriveAbsoluteEncoderOffsetRad = Units.degreesToRadians(283);// ---<Unfinished
    public static final double kBackLeftDriveAbsoluteEncoderOffsetRad = Units.degreesToRadians(24);// ---<Unfinished
    public static final double kFrontRightDriveAbsoluteEncoderOffsetRad = Units.degreesToRadians(204);// ---<Unfinished
    public static final double kBackRightDriveAbsoluteEncoderOffsetRad = Units.degreesToRadians(101);
  }

  public static final class ModuleConstants {
    public static final double kDriveMotorGearRatio = 1 / 8.16;
    public static final double kTurningMotorGearRatio = 1 / 12.8;
    public static final double kDriveEncoderRot2Meter = kDriveMotorGearRatio * Math.PI
        * RobotPositionConstants.kWheelDiameter;
    public static final double kTurningEncoderRot2Rad = kTurningMotorGearRatio * Math.PI * 2;
    public static final double kDriveEncoderRPM2MeterPerSec = kDriveEncoderRot2Meter / 60;
    public static final double kTurningEncoderRPM2RadPerSec = kTurningEncoderRot2Rad / 60;

    public static final double kPhysicalTurnMaxRadiansPerSecond = 96 * kTurningMotorGearRatio * 2 * Math.PI;

    public static final double kTurnP = 0.33;
    public static final double kTurnI = 0.0;
    public static final double kTurnD = 0.0;

    public static final double kTurnS = 0.21992;
    public static final double kTurnV = 0.26096;
    public static final double kTurnA = 0.005005;

    public static final double kDriveS = 0.028221;
    public static final double kDriveV = 7.68;// 10.715
    public static final double kDriveA = 0.73496;
  }

  public static final class RobotPositionConstants {
    public static final double kTrackWidth = Units.inchesToMeters(18.5);
    public static final double kWheelBase = Units.inchesToMeters(28.0);
    public static final double kWheelXOffset = kTrackWidth / 2;
    public static final double kWheelYOffset = kWheelBase / 2;
    public static final Translation2d kFrontLeftModuleCenter = new Translation2d(kWheelYOffset, -kWheelXOffset);
    public static final Translation2d kFrontRightModuleCenter = new Translation2d(kWheelYOffset, kWheelXOffset);
    public static final Translation2d kBackLeftModuleCenter = new Translation2d(-kWheelYOffset, -kWheelXOffset);
    public static final Translation2d kBackRightModuleCenter = new Translation2d(-kWheelYOffset, kWheelXOffset);
    public static final double kDriveFrameWidth = Units.inchesToMeters(25.0);
    public static final double kDriveFrameLength = Units.inchesToMeters(34.0);
    public static final double kBumperThickness = Units.inchesToMeters(3.0);
    public static final double kRobotWidth = kDriveFrameWidth + (2 * kBumperThickness);
    public static final double kRobotLength = kDriveFrameLength + (2 * kBumperThickness);
    public static final Transform3d kPigeonPositionFromZero = new Transform3d(
        new Translation3d(0, 0, Units.inchesToMeters(4.0625)),
        new Rotation3d(0, 0, 0));// ---<Unfinished
    public static final Translation3d kRobotCenter = new Translation3d(0, 0, 0);
    public static final Transform3d kCenterOfCameraPositionFromZero = new Transform3d(
        new Translation3d(Units.inchesToMeters(8.75), 0, Units.inchesToMeters(32.375)),
        new Rotation3d(0, Units.degreesToRadians(78), 0));
    public static final Translation3d kCenterOfArmPivotFromZero = new Translation3d(Units.inchesToMeters(18), 0,
        Units.inchesToMeters(19));
    public static final double kPivotArmLengthFromPivotPoint = Units.inchesToMeters(24);
    public static final double kClawMinimumLength = Units.inchesToMeters(8.9375);
    public static final double kClawMaximumLength = Units.inchesToMeters(13.25);
    public static final double kMinimumExtension = Units.inchesToMeters(0);
    public static final double kMaximumExtension = Units.inchesToMeters(Units.inchesToMeters(29));
    public static final double kCenterOfAverageGamePieceFromExtension = Units.inchesToMeters(6.25);
    public static final double kWheelDiameter = Units.inchesToMeters(4);

    public static final double kMaxHeightDuringMatch = Units.inchesToMeters(78);
    public static final double kMaxExtensionDuringMatch = Units.inchesToMeters(48);
    public static final double kMaxYFromPivot = kMaxHeightDuringMatch - kCenterOfArmPivotFromZero.getZ();
    public static final double kMinYFromPivot = -kCenterOfArmPivotFromZero.getZ() + Units.inchesToMeters(1);
    public static final double kMaxXFromPivot = kMaxExtensionDuringMatch + kCenterOfArmPivotFromZero.getX();
    public static final double kMinXFromPivot = -kMaxExtensionDuringMatch
        - (kDriveFrameLength - kCenterOfArmPivotFromZero.getX());
  }

  public static final class FieldPositionConstants {
    public static final class nodePositions {

      private static final double highrungHeight = 1.16;
      private static final double midrungHeight = 0.86;
      private static final double floorHeight = 0.0;

      private static final double redXTranslation = 0.0;// ---<Unfinished
      private static final double blueXTranslation = 0.0;// ---<Unfinished

      // column 1-9 numbered top to bottom from field image
      private static final double column1YTranslation = 0.0;// ---<Unfinished
      private static final double column2YTranslation = 0.0;// ---<Unfinished
      private static final double column3YTranslation = 0.0;// ---<Unfinished
      private static final double column4YTranslation = 0.0;// ---<Unfinished
      private static final double column5YTranslation = 0.0;// ---<Unfinished
      private static final double column6YTranslation = 0.0;// ---<Unfinished
      private static final double column7YTranslation = 0.0;// ---<Unfinished
      private static final double column8YTranslation = 0.0;// ---<Unfinished
      private static final double column9YTranslation = 0.0;// ---<Unfinished

      public static enum Nodes {
        redG1R1C1(new Translation3d(redXTranslation, column1YTranslation, highrungHeight)),
        redG1R1C2(new Translation3d(redXTranslation, column2YTranslation, highrungHeight)),
        redG1R1C3(new Translation3d(redXTranslation, column3YTranslation, highrungHeight)),
        redG1R2C1(new Translation3d(redXTranslation, column1YTranslation, midrungHeight)),
        redG1R2C2(new Translation3d(redXTranslation, column2YTranslation, midrungHeight)),
        redG1R2C3(new Translation3d(redXTranslation, column3YTranslation, midrungHeight)),
        redG1R3C1(new Translation3d(redXTranslation, column1YTranslation, floorHeight)),
        redG1R3C2(new Translation3d(redXTranslation, column2YTranslation, floorHeight)),
        redG1R3C3(new Translation3d(redXTranslation, column3YTranslation, floorHeight)),

        redG2R1C1(new Translation3d(redXTranslation, column4YTranslation, highrungHeight)),
        redG2R1C2(new Translation3d(redXTranslation, column5YTranslation, highrungHeight)),
        redG2R1C3(new Translation3d(redXTranslation, column6YTranslation, highrungHeight)),
        redG2R2C1(new Translation3d(redXTranslation, column4YTranslation, midrungHeight)),
        redG2R2C2(new Translation3d(redXTranslation, column5YTranslation, midrungHeight)),
        redG2R2C3(new Translation3d(redXTranslation, column6YTranslation, midrungHeight)),
        redG2R3C1(new Translation3d(redXTranslation, column4YTranslation, floorHeight)),
        redG2R3C2(new Translation3d(redXTranslation, column5YTranslation, floorHeight)),
        redG2R3C3(new Translation3d(redXTranslation, column6YTranslation, floorHeight)),

        redG3R1C1(new Translation3d(redXTranslation, column7YTranslation, highrungHeight)),
        redG3R1C2(new Translation3d(redXTranslation, column8YTranslation, highrungHeight)),
        edG3R1C3(new Translation3d(redXTranslation, column9YTranslation, highrungHeight)),
        redG3R2C1(new Translation3d(redXTranslation, column7YTranslation, midrungHeight)),
        redG3R2C2(new Translation3d(redXTranslation, column8YTranslation, midrungHeight)),
        redG3R2C3(new Translation3d(redXTranslation, column9YTranslation, midrungHeight)),
        redG3R3C1(new Translation3d(redXTranslation, column7YTranslation, floorHeight)),
        redG3R3C2(new Translation3d(redXTranslation, column8YTranslation, floorHeight)),
        redG3R3C3(new Translation3d(redXTranslation, column9YTranslation, floorHeight)),

        blueG1R1C1(new Translation3d(blueXTranslation, column1YTranslation, highrungHeight)),
        blueG1R1C2(new Translation3d(blueXTranslation, column2YTranslation, highrungHeight)),
        blueG1R1C3(new Translation3d(blueXTranslation, column3YTranslation, highrungHeight)),
        blueG1R2C1(new Translation3d(blueXTranslation, column1YTranslation, midrungHeight)),
        blueG1R2C2(new Translation3d(blueXTranslation, column2YTranslation, midrungHeight)),
        blueG1R2C3(new Translation3d(blueXTranslation, column3YTranslation, midrungHeight)),
        blueG1R3C1(new Translation3d(blueXTranslation, column1YTranslation, floorHeight)),
        blueG1R3C2(new Translation3d(blueXTranslation, column2YTranslation, floorHeight)),
        blueG1R3C3(new Translation3d(blueXTranslation, column3YTranslation, floorHeight)),

        blueG2R1C1(new Translation3d(blueXTranslation, column4YTranslation, highrungHeight)),
        blueG2R1C2(new Translation3d(blueXTranslation, column5YTranslation, highrungHeight)),
        blueG2R1C3(new Translation3d(blueXTranslation, column6YTranslation, highrungHeight)),
        blueG2R2C1(new Translation3d(blueXTranslation, column4YTranslation, midrungHeight)),
        blueG2R2C2(new Translation3d(blueXTranslation, column5YTranslation, midrungHeight)),
        blueG2R2C3(new Translation3d(blueXTranslation, column6YTranslation, midrungHeight)),
        blueG2R3C1(new Translation3d(blueXTranslation, column4YTranslation, floorHeight)),
        blueG2R3C2(new Translation3d(blueXTranslation, column5YTranslation, floorHeight)),
        blueG2R3C3(new Translation3d(blueXTranslation, column6YTranslation, floorHeight)),

        blueG3R1C1(new Translation3d(blueXTranslation, column7YTranslation, highrungHeight)),
        blueG3R1C2(new Translation3d(blueXTranslation, column8YTranslation, highrungHeight)),
        blueG3R1C3(new Translation3d(blueXTranslation, column9YTranslation, highrungHeight)),
        blueG3R2C1(new Translation3d(blueXTranslation, column7YTranslation, midrungHeight)),
        blueG3R2C2(new Translation3d(blueXTranslation, column8YTranslation, midrungHeight)),
        blueG3R2C3(new Translation3d(blueXTranslation, column9YTranslation, midrungHeight)),
        blueG3R3C1(new Translation3d(blueXTranslation, column7YTranslation, floorHeight)),
        blueG3R3C2(new Translation3d(blueXTranslation, column8YTranslation, floorHeight)),
        blueG3R3C3(new Translation3d(blueXTranslation, column9YTranslation, floorHeight));

        public Translation3d position;

        Nodes(Translation3d position) {
          position = this.position;
        }
      }
    }

    public static enum pathTrajectories {
      blueLeftFull(
          PathPlanner.loadPathGroup(pathNames.blueLeftFull.pathName, MovementConstants.kPathPlannerConstraints)),
      blueLeftMinimum(PathPlanner.loadPathGroup(pathNames.blueLeftFull.pathName,
          MovementConstants.kPathPlannerConstraints)),
      blueCenterLeftFull(PathPlanner.loadPathGroup(pathNames.blueLeftFull.pathName,
          MovementConstants.kPathPlannerConstraints)),
      blueCenterRightFull(PathPlanner.loadPathGroup(pathNames.blueLeftFull.pathName,
          MovementConstants.kPathPlannerConstraints)),
      blueCenterMinimum(PathPlanner.loadPathGroup(pathNames.blueLeftFull.pathName,
          MovementConstants.kPathPlannerConstraints)),
      blueRightFull(PathPlanner.loadPathGroup(pathNames.blueLeftFull.pathName,
          MovementConstants.kPathPlannerConstraints)),
      blueRightMinimum(PathPlanner.loadPathGroup(pathNames.blueLeftFull.pathName,
          MovementConstants.kPathPlannerConstraints)),

      // WARNING DO NOT USE BUILT IN CONVERSION TOOL FOR SWAPPING ALLIANCE SIDES, IT
      // WAS DONE POORLY AND DOES NOT ACCOUNT FOR ABSOLUTE POSITION OF APRIL TAGS,
      // STAY AWAY
      redLeftFull(null),
      redLeftMinimum(PathPlanner.loadPathGroup(pathNames.blueLeftFull.pathName,
          MovementConstants.kPathPlannerConstraints)),
      redCenterLeftFull(PathPlanner.loadPathGroup(pathNames.blueLeftFull.pathName,
          MovementConstants.kPathPlannerConstraints)),
      redCenterRightFull(PathPlanner.loadPathGroup(pathNames.blueLeftFull.pathName,
          MovementConstants.kPathPlannerConstraints)),
      redCenterMinimum(PathPlanner.loadPathGroup(pathNames.blueLeftFull.pathName,
          MovementConstants.kPathPlannerConstraints)),
      redRightFull(PathPlanner.loadPathGroup(pathNames.blueLeftFull.pathName,
          MovementConstants.kPathPlannerConstraints)),
      redRightMinimum(
          PathPlanner.loadPathGroup(pathNames.blueLeftFull.pathName, MovementConstants.kPathPlannerConstraints));

      private List<PathPlannerTrajectory> trajectories;

      pathTrajectories(List<PathPlannerTrajectory> trajectories) {
        this.trajectories = trajectories;
      }

      public List<PathPlannerTrajectory> trajectories() {
        return trajectories;
      }

    }

    public static enum pathNames {
      blueLeftFull("topFull");

      private String pathName;

      pathNames(String pathName) {
        this.pathName = pathName;
      }

      public String pathName() {
        return pathName;
      }
    }

    public static List<PathPlannerTrajectory> convertBluePathToRed() {
      PathPlannerTrajectory.transformTrajectoryForAlliance(null, Alliance.Red);
      return null;
    }

    public static enum scoringPositions {
      redLeftCube(new Pose2d(14.75, 1.075, Rotation2d.fromDegrees(0))),
      redLeftCone(new Pose2d(14.75, 1.65, Rotation2d.fromDegrees(0))),
      redCenterLeftCone(new Pose2d(14.75, 2.225, Rotation2d.fromDegrees(0))),
      redCenterCube(new Pose2d(14.75, 2.775, Rotation2d.fromDegrees(0))),
      redCenterRightCone(new Pose2d(14.75, 3.3, Rotation2d.fromDegrees(0))),
      redRightCone(new Pose2d(14.75, 3.865, Rotation2d.fromDegrees(0))),
      redRightCube(new Pose2d(14.75, 4.425, Rotation2d.fromDegrees(0))),

      blueLeftCube(new Pose2d(1.9, 4.425, Rotation2d.fromDegrees(180))),
      blueLeftCone(new Pose2d(1.9, 3.865, Rotation2d.fromDegrees(180))),
      blueCenterLeftCone(new Pose2d(1.9, 3.3, Rotation2d.fromDegrees(180))),
      blueCenterCube(new Pose2d(1.9, 2.775, Rotation2d.fromDegrees(180))),
      blueCenterRightCone(new Pose2d(1.9, 2.225, Rotation2d.fromDegrees(180))),
      blueRightCone(new Pose2d(1.9, 1.65, Rotation2d.fromDegrees(180))),
      blueRightCube(new Pose2d(1.9, 1.075, Rotation2d.fromDegrees(180)));

      private Pose2d pose;

      scoringPositions(Pose2d pose) {
        this.pose = pose;
      }

      public Pose2d pose() {
        return pose;
      }
    }

    public static enum grabbingPositions {
      redLeft(new Pose2d(10, 0.9, Rotation2d.fromDegrees(180))),
      redCenterLeft(new Pose2d(10, 2.1, Rotation2d.fromDegrees(180))),
      redCenterRight(new Pose2d(10, 3.35, Rotation2d.fromDegrees(180))),
      redRight(new Pose2d(10, 4.575, Rotation2d.fromDegrees(180))),

      blueLeft(new Pose2d(6.5, 4.575, Rotation2d.fromDegrees(0))),
      blueCenterLeft(new Pose2d(6.5, 3.35, Rotation2d.fromDegrees(0))),
      blueCenterRight(new Pose2d(6.5, 2.1, Rotation2d.fromDegrees(0))),
      blueRight(new Pose2d(6.5, 0.9, Rotation2d.fromDegrees(0)));

      private Pose2d pose;

      grabbingPositions(Pose2d pose) {
        this.pose = pose;
      }

      public Pose2d pose() {
        return pose;
      }
    }

    public static enum chargeStationPositions {
      redLeft(new Pose2d(12.6, 2.0, Rotation2d.fromDegrees(180))),
      redCenter(new Pose2d(12.6, 2.75, Rotation2d.fromDegrees(180))),
      redRight(new Pose2d(12.6, 3.5, Rotation2d.fromDegrees(180))),
      blueLeft(new Pose2d(3.9, 3.5, Rotation2d.fromDegrees(0))),
      blueCenter(new Pose2d(3.9, 2.75, Rotation2d.fromDegrees(0))),
      blueRight(new Pose2d(3.9, 2.0, Rotation2d.fromDegrees(0)));

      private Pose2d pose;

      chargeStationPositions(Pose2d pose) {
        this.pose = pose;
      }

      public Pose2d pose() {
        return pose;
      }
    }

    public static enum startingRobotPositions {
      redLeft(new Pose2d(0, 0, Rotation2d.fromDegrees(0))),
      redCenter(new Pose2d(0, 0, Rotation2d.fromDegrees(0))),
      redRight(new Pose2d(0, 0, Rotation2d.fromDegrees(0))),
      blueLeft(new Pose2d(0, 0, Rotation2d.fromDegrees(180))),
      blueCenter(new Pose2d(0, 0, Rotation2d.fromDegrees(180))),
      blueRight(new Pose2d(0, 0, Rotation2d.fromDegrees(180)));

      private final Pose2d pose;

      startingRobotPositions(Pose2d pose) {
        this.pose = pose;
      }

      public Pose2d pose() {
        return pose;
      }
    }

    public static final class aprilTagPositions {
      private static final double aprilTagGridHeightMeters = Units.inchesToMeters(18.22);
      private static final double aprilTagSubstationHeightMeters = Units.inchesToMeters(27.38);
      // Clockwise from the Red(Right) Side
      // Red1Cube
      public static final Pose3d ID1 = new Pose3d(
          Units.inchesToMeters(610.77),
          Units.inchesToMeters(42.19),
          aprilTagGridHeightMeters,
          new Rotation3d(0, 0, Math.PI));
      // Red2Cube
      public static final Pose3d ID2 = new Pose3d(
          Units.inchesToMeters(610.77),
          Units.inchesToMeters(108.19),
          aprilTagGridHeightMeters,
          new Rotation3d(0, 0, Math.PI));
      // Red3Cube
      public static final Pose3d ID3 = new Pose3d(
          Units.inchesToMeters(610.77),
          Units.inchesToMeters(174.19),
          aprilTagGridHeightMeters,
          new Rotation3d(0, 0, Math.PI));
      // BlueSubstation
      public static final Pose3d ID4 = new Pose3d(
          Units.inchesToMeters(636.96),
          Units.inchesToMeters(265.74),
          aprilTagSubstationHeightMeters,
          new Rotation3d(0, 0, Math.PI));
      // RedSubstation
      public static final Pose3d ID5 = new Pose3d(
          Units.inchesToMeters(14.25),
          Units.inchesToMeters(265.74),
          aprilTagSubstationHeightMeters,
          new Rotation3d(0, 0, 0));
      // Blue1Cube
      public static final Pose3d ID6 = new Pose3d(
          Units.inchesToMeters(40.45),
          Units.inchesToMeters(174.19),
          aprilTagGridHeightMeters,
          new Rotation3d(0, 0, 0));
      // Blue2Cube
      public static final Pose3d ID7 = new Pose3d(
          Units.inchesToMeters(40.45),
          Units.inchesToMeters(108.19),
          aprilTagGridHeightMeters,
          new Rotation3d(0, 0, 0));
      // Blue3Cube
      public static final Pose3d ID8 = new Pose3d(
          Units.inchesToMeters(40.45),
          Units.inchesToMeters(42.19),
          aprilTagGridHeightMeters,
          new Rotation3d(0, 0, 0));
    }
  }
}
